/**
 * @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: GenerateRooms.java
 * Short Description: Generate Rooms for Final Project
 **/
import java.awt.*;

public class GenerateRooms {
    Rectangle mainFrameBounds;

    Rectangle[] corridorBounds = new Rectangle[1000];
    Rectangle[] corridorBounds2 = new Rectangle[1000];
    Rectangle[] allRooms;

    Boolean deBug = false;

    GenerateRooms(Rectangle varFrameBounds) {
        if (deBug) System.out.println("Creating new GenerateRoom Object");
        mainFrameBounds = varFrameBounds;

    }
    // Create and return a random int. Pass a range to it
    int makeRandomInt(double range)
    {
        // create random value
        double r = Math.random();
        // create new value
        int myrandomNumber;
        // Create range between 0 and 5
        myrandomNumber = (int)(r*range);
        // Return value
        return myrandomNumber;
    }

    // Create rooms
    Rectangle[] genRooms(int roomNumber) {
        // make new array for all rooms
        allRooms = new Rectangle[roomNumber];

        // make counter for new rooms
        int totalRooms = 0;
        // make five rooms
        while (totalRooms <= (roomNumber - 1)) {
            // Print out which room we're making
            if (deBug) System.out.println("Room Generation number " + totalRooms);

            // Create random values for the rooms coordinates
            int x = makeRandomInt(1000);
            int y = makeRandomInt(700);
            // make the width and height of boxes at least 100
            int width = makeRandomInt(100) + 100;
            int height = makeRandomInt(100) + 100;
            // Make a new rectangle for room
            Rectangle newBox = new Rectangle(x, y, width, height);
            // Set roomIntersects to false, then check
            Boolean roomIntersects = false;
            // Print out some debug coordinates
            if (deBug) System.out.println("Rectangle bottom corner coord: " + (newBox.height + y + 5) + " < 720 (" + mainFrameBounds.height + ")");
            if (deBug) System.out.println("Rectangle right corner coord: " + (newBox.width + x + 5) + " < " + mainFrameBounds.width);
            // check first rectangle is inside the main frame
            // if height of box + 5 padding < frame height and box width + 5 padding < frame width
            if ((newBox.height + y + 15) < mainFrameBounds.height && ((newBox.width + x + 15) < mainFrameBounds.width)) {
                // Print out witch room we're adding
                if (deBug) System.out.println("Creating additional rooms " + totalRooms);
                // Check each room's rectangle for intersection with the new rectangle
                for (Rectangle u : allRooms) {
                    // Print out rooms we're checking
                    if (deBug) System.out.println("Checking allRooms " + u + " against " + newBox);
                    // if the Rectangle array is not null...
                    if (u != null) {
                        // Print it out so we know
                        if (deBug) System.out.println(u + " is not null");
                        // If the new box intersects any of the boxes in the array
                        // don't use it
                        if (newBox.intersects(u)) {
                            if (newBox.height + x >= mainFrameBounds.height) {
                                if (deBug) System.out.print(" ##### This Box Should Be Outside MainFrame! & ");
                            }
                            if (newBox.width + x >= mainFrameBounds.width) {
                                if (deBug) System.out.print(" ##### This Box Should Be Outside MainFrame! & ");
                            }
                             if (deBug) System.out.println("Boxes intersect");
                            roomIntersects = true;
                        } else {
                             if (deBug) System.out.println("Boxes do not intersect");
                        }
                    } else {
                         if (deBug) System.out.println("allRooms is null");
                    }
                }
                if (!roomIntersects) {
                    allRooms[totalRooms] = newBox;
                    totalRooms += 1;
                } else {
                     if (deBug) System.out.println("Something intersects");
                }
            }

        }
        // get first rooms coordinates
        for (int i = 0; i < (allRooms.length - 1); i++) {
            int box1centerX = (int) allRooms[i].getCenterX();
            int box1centerY = (int) allRooms[i].getCenterY();
            int box2centerX = (int) allRooms[i + 1].getCenterX();
            int box2centerY = (int) allRooms[i + 1].getCenterY();
            int box1maxX = (int) allRooms[i].getMaxX();
            int box1maxY = (int) allRooms[i].getMaxY();
            int box2maxX = (int) allRooms[i + 1].getMaxX();
            int box2maxY = (int) allRooms[i + 1].getMaxY();
            int box1X = (int) allRooms[i].getX();
            int box1Y = (int) allRooms[i].getY();
            int box2X = (int) allRooms[i + 1].getX();
            int box2Y = (int) allRooms[i + 1].getY();


            Rectangle newCorridor = new Rectangle(0,0,0,0);
            Rectangle newCorridor2 = new Rectangle(0,0,0,0);

            if (deBug) System.out.println("Room number " + i + " and " + (i + 1));
            if (deBug) System.out.println("box1centerX: " + box1centerX
                    + "\nbox1centerY: " + box1centerY
                    + "\nbox2centerX: " + box2centerX
                    + "\nbox2centerY: " + box2centerY
                    + "\nbox1maxX: " + box1maxX
                    + "\nbox1maxY: " + box1maxY
                    + "\nbox2maxX: " + box2maxX
                    + "\nbox2maxY: " + box2maxY
                    + "\nbox1X: " + box1X
                    + "\nbox1Y: " + box1Y
                    + "\nbox2X: " + box2X
                    + "\nbox2Y: " + box2Y);
            // Create some boolean to find access box location
            Boolean box2Right;
            Boolean box2Down;

            // if center X 1 is greater than center X 2
            if (box1centerX > box2centerX) {
                // box 2 is left of box 1
                box2Right = false;
                // if center X 1 is greater than center Y 2
                box2Down = box1centerY <= box2centerY;
            } else {
                box2Down = box1centerY <= box2centerY;
                box2Right = true;
            }
            if (box2Down && box2Right) {
                if (deBug) System.out.println("Box " + allRooms[i + 1] + " is right and down : ");
                if (deBug) System.out.println("Box is direct south: " + ((box1maxX - box2X) > 55) + " Box is direct east: " + ((box1maxY - box2Y) > 65));
                //Boxes are close enough for a direct connection


                // if direct south
                if (box2X > box1X && (box1maxX - box2X) > 55) {
                    if (deBug) System.out.println("box2X > box1X - Corridor : " + (box2X + 5) + " " + box1maxY + " " + 50 + " " + (box2Y - box1maxY));
                    newCorridor.setBounds((box2X + 5), box1maxY, 50, (box2Y - box1maxY));
                }
                if (box2X < box1X && (box1X + 55) < box2maxX) {
                    if (deBug) System.out.println("box2X < box1X - Corridor : " + (box1X + 5) + " " + box1maxY + " " + 50 + " " + (box2Y - box1maxY));
                    newCorridor.setBounds((box1X + 5), box1maxY, 50, (box2Y - box1maxY));
                }

                // if direct east
                if ((box2Y >= box1Y) && (box1maxY - box2Y) > 65) {
                    if (deBug) System.out.println("box2Y > box1Y - Corridor : " + box1maxX + " " + (box2Y + 5) + " " + (box1X - box2maxX) + " " + 60);
                    newCorridor.setBounds(box1maxX, (box2Y + 5), (box2X - box1maxX), 60);
                }
                if (box2Y <= box1Y && (box1Y + 65) < box1maxY) {
                    if (deBug) System.out.println("box2Y < box1Y : " + box1maxX + " " + (box2Y + 5) + " " + (box1X - box2maxX) + " " + 60);
                    newCorridor.setBounds(box1maxX, box1Y + 5, (box2X - box1maxX), 60);
                }
                if (!((box1maxX - box2X) > 55) && !((box1maxY - box2Y) > 65)) {
                    if (deBug) System.out.println("create two corridors between middle of rooms");
                    // go right
                    newCorridor.setBounds(box1maxX, (box1centerY - 30), ((box2centerX + 25) - box1maxX), 60);
                    // go down
                    newCorridor2.setBounds((box2centerX - 25), ((box1centerY + 30)), 50, (box2Y - (box1centerY + 25)));
                }

            }

            if (box2Down && !box2Right) {
                if (deBug) System.out.println("Box " + allRooms[i + 1] + " is left and down");
                if (deBug) System.out.println("Box is direct south: " + ((box2maxX - 55) > box1X) + " Box is direct west: " + ((box1maxY - 65) > box2Y));

                // if direct south
                if (box1X > box2X && (box2maxX - 55) > box1X) {
                    if (deBug) System.out.println("box1X > box2X - Corridor : " + box2X + " " + box1maxY + " " + 50 + " " + (box2Y - box1maxY));
                    newCorridor.setBounds((box1X + 5), box1maxY, 50, (box2Y - box1maxY));
                }
                if (box1X < box2X && (box2X + 55) < box1maxX) {
                    if (deBug)
                        System.out.println("box2X < box1X - Corridor : " + box2X + " " + box1maxY + " " + 50 + " " + (box2Y - box1maxY));
                    newCorridor.setBounds(box2X + 5, box1maxY, 50, (box2Y - box1maxY));
                }

                // if direct west
                if (box1Y < box2Y && (box1maxY - 65) > box2Y) {
                    if (deBug) System.out.println("box2Y < box1maxY - Corridor : " + box2maxX + " " + (box2Y + 5) + " " + (box1X - box2maxX) + " " + 60);
                    newCorridor.setBounds(box2maxX, box2Y + 5, (box1X - box2maxX), 60);
                }
                if (box1Y > box2Y && (box2Y + 65) < box1maxY) {
                    if (deBug) System.out.println("box1Y > box2Y - Corridor : " + box2maxX + " " + (box2Y + 5) + " " + (box1X - box2maxX) + " " + 60);
                    newCorridor.setBounds(box2maxX, box1Y + 5, (box1X - box2maxX), 60);
                }

                if (!((box2maxX - 55) > box1X) && !((box1maxY - 65) > box2Y)) {
                    if (deBug) System.out.println("create two corridors between middle of rooms");

                    // go left
                    if (deBug) System.out.println("Go Left " + (box2centerX - 25) +" "+ (box1centerY - 30)  +" "+  (box1X - (box2centerX - 25)) +" "+  60);
                    newCorridor.setBounds(box2centerX - 25, (box1centerY - 30), (box1X - (box2centerX - 25)), 60);
                    // go down
                    if (deBug) System.out.println("Go down " + (box2centerX - 25) +" "+ (box1centerY + 30)  +" "+  50 +" "+  (box2Y - (box1centerY + 30)));
                    newCorridor2.setBounds((box2centerX - 25), (box1centerY + 30), 50, (box2Y - (box1centerY + 30)));
                }
            }
            if (!box2Down && box2Right) {
                if (deBug) System.out.println("Box " + allRooms[i + 1] + " is right and up");
                if (deBug) System.out.println("Box is direct north: " + ((box1maxX - box2X) > 55) + " Box is direct east: " + ((box2maxY - box1Y) > 65));


                // if direct north
                //if (box2X < box1maxX && (box2X + 55) < box1maxX) {
                if (box1X < box2X && (box2X + 55) < box1maxX) {
                    if (deBug) System.out.println("box1X < box2X - Corridor : " + box2X + 5 + " " + box1maxY + " " + 50 + " " + (box2Y - box1maxY));
                    newCorridor.setBounds((box2X + 5), box2maxY, 50, (box1Y - box2maxY));
                }
                if (box1X > box2X && (box1X + 55) < box2maxX) {
                    if (deBug) System.out.println("box1X < box2X - Corridor : " + box2X + 5 + " " + box2maxY + " " + 50 + " " + (box2Y - box1maxY));
                    newCorridor.setBounds((box1X + 5), box2maxY, 50, (box1Y - box2maxY));
                }
                // if direct east
                if ((box2maxY - 65) > box1Y && (box2Y > box1Y)) {
                    if (deBug) System.out.println("(box2maxY - 55) > box1Y && box2Y < box1Y - Corridor : " + box1maxX + " " + (box2Y + 5) + " " + (box1X - box2maxX) + " " + 60);
                    newCorridor.setBounds(box1maxX, box2Y + 5, (box2X - box1maxX), 60);
                }
                if ((box2maxY - box1Y) > 65) {
                    if (deBug) System.out.println("Else - Corridor : " + box1maxX + " " + (box2Y + 5) + " " + (box1X - box2maxX) + " " + 60);
                    newCorridor.setBounds(box1maxX, box1Y + 5, (box2X - box1maxX), 60);
                }

                if (!((box1maxX - box2X) > 55) && !((box2maxY - box1Y) > 65)) {
                    if (deBug) System.out.println("create two corridors between middle of rooms");
                    // go right
                    newCorridor.setBounds(box1maxX, (box1centerY - 30), ((box2centerX + 25) - box1maxX), 60);
                    // go up
                    newCorridor2.setBounds((box2centerX - 25), (box2maxY), 50, ((box1centerY - 30) - (box2maxY)));
                }
            }

            if (!box2Down && !box2Right) {
                if (deBug) System.out.println("Box " + allRooms[i + 1] + " is left and up");
                if (deBug) System.out.println("Box is direct north: " + ((box2maxX - box1X) > 55) + " Box is direct west: " + ((box2maxY - box1Y) > 65));
                //Boxes are close enough for a direct connection

                // if direct north
                //if (box2X < box1maxX && (box2X + 55) < box1maxX) {
                if (box2maxX < box1maxX && (box2maxX - box1X) > 55) {
                    if (deBug) System.out.println("(box2maxX - 55) > box1X - Corridor : " + (box2maxX - 55) + " " + box2maxY + " " + 50 + " " + (box2maxY - box1Y));
                    newCorridor.setBounds(box2maxX - 55, box2maxY, 50, (box1Y - box2maxY));
                }
                if (box2maxX > box1maxX && (box2maxX - box1X) > 55) {
                    if (deBug) System.out.println("(box2maxX - 55) > box1X - Corridor : " + (box2maxX - 55) + " " + box2maxY + " " + 50 + " " + (box2maxY - box1Y));
                    newCorridor.setBounds(box1maxX - 55, box2maxY, 50, (box1Y - box2maxY));
                }

                // if direct west
                if (box2maxY < box1maxY && (box2maxY - 65) > box1Y) {
                    if (deBug) System.out.println("box2maxY < box1maxY - Corridor : " + box2maxX + " " + (box2Y + 5) + " " + (box1X - box2maxX) + " " + 60);
                    newCorridor.setBounds(box2maxX, box2maxY - 65, (box1X - box2maxX), 60);
                }
                if (box2maxY > box1maxY && (box1maxY - 65) > box2Y) {
                    if (deBug) System.out.println("box2maxY > box1maxY - Corridor : " + box2maxX + " " + (box2Y + 5) + " " + (box1X - box2maxX) + " " + 60);
                    newCorridor.setBounds(box2maxX, box1maxY - 65, (box1X - box2maxX), 60);
                }


                if (!((box2maxX - box1X) > 55) && !((box2maxY - box1Y) > 65)) {
                    if (deBug) System.out.println("create two corridors between middle of rooms");

                    // go left
                    newCorridor.setBounds((box2centerX - 25), (box1centerY - 30), (box1X - (box2centerX - 25)), 60);
                    // go down
                    newCorridor2.setBounds((box2centerX - 25), (box2maxY), 50, ((box1centerY - 30) - box2maxY));
                }
            }

            corridorBounds[i] = newCorridor.getBounds();
            corridorBounds2[i] = newCorridor2.getBounds();
        }

        return allRooms;
    }
    Rectangle[] getCorridorBounds() {
        return corridorBounds;
    }
    Rectangle[] getCorridorBounds2() {
        return corridorBounds2;
    }
}
