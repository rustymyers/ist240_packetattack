/**
* @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: InstructionPane.java
 * Short Description: Instructions Panel for Final Project
**/
import java.awt.*;
import java.io.File;
import javax.swing.*;

public class InstructionPane extends JPanel
{
    Boolean deBug = false;
	// class parameters
	public JButton backButton;
    XML_240 xmlSave;
    String xmlFile = "PacketAttackHighScores.xml";
    JButton upStairsTile, downStairsTile;
    JLabel highScores, backgroundLabel;

	// JButton startButton;
	public InstructionPane()
	{
		// create new panel from superclass
		super();
		JLabel instructions;
        highScores = getHighScores(xmlFile);
		// create local variable for use
		Dimension size;
		// set layout to border for jlabel
		setLayout(new BorderLayout());
		// Create image icon from local file
		ImageIcon backgroundImage = new ImageIcon(this.getClass().getResource("images/Flat_earth_night.jpg"));
		// Add image to JLabel
        backgroundLabel = new JLabel(backgroundImage);
		// Add new castle image jlabel to menu panel
		add(backgroundLabel);
		// set castle image jlabel layout to Null
		backgroundLabel.setLayout(null);
		// create back button
		backButton = new JButton("Back");
		// add back button to castle jlabel
		backgroundLabel.add(backButton);
		//
		// END DEFAULT WORK
		//
		
		//
		// START CUSTOM PANEL WORK
		// create instructions jlabel
		instructions = new JLabel(
            //Broken up by line for readability when changing
            "<html><b>How To Play</b> [version 1.0]<br><br>"
            + "<b>Start Game:</b>"
            + "<br>"
            + " This button will start a new game.<br>"
            + "<br>"
            + "<b>Movement:</b></br>"
            + "<p>Move your character with the arrow keys.</p></br>"
            + "<p>Attack with the number key 1</p></br>"
            + "Note: Attack only works when near enemy<br>"
            + "<b>Settings:</b></br>"
            + "<p>Configure the game to play how you want.</p></br>"
            + "<br>"
            + "</html>"
			);
		// Sets jlabel font color
		instructions.setForeground(Color.WHITE);
		// Add instructions jlabel to castle jlabel
		backgroundLabel.add(instructions);
		// set size and placement of instructions label- (starting x, starting y, x width, y width)
        instructions.setVerticalAlignment(JLabel.TOP);
        // get prefered size of back button
        size = backButton.getPreferredSize();
        // set size and placement of back button- (starting x, starting y, x width, y width)
        backButton.setBounds(640-(size.width/2),630,size.width, size.height);



        // create label for treasure
        JLabel treasure = createLabel("<html><font size=\"+1\"><b>Treasure</b></font></html>");
        JLabel healthPotion = createLabel("<html><b>Health Potion</b><br>Increase Hero Health</html>");
        JLabel attackPotion = createLabel("<html><b>Speed Potion</b><br>Increase Attack Speed</html>");
        JLabel wifiTreasure = createLabel("<html><b>5Ghz Wireless</b><br>Skip a level</html>");
        JLabel portFastTreasure = createLabel("<html><b>PortFast Packet</b><br>Increase Attack Damage</html>");

        backgroundLabel.add(healthPotion);
        backgroundLabel.add(attackPotion);
        backgroundLabel.add(wifiTreasure);
        backgroundLabel.add(portFastTreasure);
        backgroundLabel.add(treasure);


        Treasure health = new Treasure("Health Potion", 0);
        Treasure speed = new Treasure("Attack Speed Potion", 1);
        Treasure wireless = new Treasure("5Ghz WiFi Signal", 2);
        Treasure packet = new Treasure("PortFast Packet", 3);

        backgroundLabel.add(health);
        backgroundLabel.add(speed);
        backgroundLabel.add(wireless);
        backgroundLabel.add(packet);


        JLabel monsters = createLabel("<html><font size=\"+1\"><b>Monsters</b></font></html>");
        JLabel firmwareLabel = createLabel("<html><font size=\"+1\"><b>Firmware Bug</b></font></html>");
        JLabel virusLabel = createLabel("<html><font size=\"+1\"><b>Virus</b></font></html>");
        JLabel firewallLabel = createLabel("<html><font size=\"+1\"><b>Firewall</b></font></html>");
        JLabel drevilLabel = createLabel("<html><font size=\"+1\"><b>Dr. Evil Packet</b></font></html>");

        backgroundLabel.add(firmwareLabel);
        backgroundLabel.add(virusLabel);
        backgroundLabel.add(firewallLabel);
        backgroundLabel.add(monsters);
        backgroundLabel.add(drevilLabel);


        Monster firmware = new Monster("Firmware Bug", 100, 0);
        Monster virus = new Monster("Virus", 100, 1);
        Monster firewall = new Monster("Firewall", 100, 2);
        Monster drEvil = new Monster("Dr. Evil Packet", 100, true);

        backgroundLabel.add(firmware);
        backgroundLabel.add(virus);
        backgroundLabel.add(firewall);
        backgroundLabel.add(drEvil);



        JLabel upStairsLabel = createLabel("<html><b>Up Stairs</b></font></html>");
        JLabel downStairsLabel = createLabel("<html><b>Down Stairs</b></font></html>");

        upStairsTile = new JButton(new ImageIcon(this.getClass().getResource("images/gamestairs3.png")));
        downStairsTile = new JButton(new ImageIcon(this.getClass().getResource("images/gamestairs4.png")));

        backgroundLabel.add(upStairsLabel);
        backgroundLabel.add(downStairsLabel);
        backgroundLabel.add(upStairsTile);
        backgroundLabel.add(downStairsTile);





        // move that stuff! Move it good!
        // instructions
        instructions.setBounds(800,100,400,400);

        // treasure label
        treasure.setBounds(290,100,200,40);
        // treasure items
        health.setLocation(230,170);
        speed.setLocation(340,170);
        wireless.setLocation(440,170);
        packet.setLocation(540,170);
        // treasure labels
        healthPotion.setBounds(200,220,90,80);
        attackPotion.setBounds(300,220,100,80);
        wifiTreasure.setBounds(400,220,100,80);
        portFastTreasure.setBounds(500,220,100,80);

        upStairsTile.setBounds(320,480,40,30);
        upStairsLabel.setBounds(310,520,60,60);
        downStairsTile.setBounds(420,480,40,30);
        downStairsLabel.setBounds(400,520,80,60);

        // monsters heading
        monsters.setBounds(290,300,200,40);
        // images
        firmware.setLocation(200,340);
        virus.setLocation(300,340);
        firewall.setLocation(420,340);
        drEvil.setLocation(520,340);
        // labels
        firmwareLabel.setBounds(160,420,130,130);
        virusLabel.setBounds(270,420,130,130);
        firewallLabel.setBounds(390,420,130,130);
        drevilLabel.setBounds(490,420,130,130);

    }

    void updateHighScores() {
        System.out.println("Getting updated high scores");
        backgroundLabel.remove(highScores);
        highScores = getHighScores(xmlFile);
        backgroundLabel.add(highScores);
        highScores.setBounds(800,200,400,400);
    }

    JLabel createLabel(String newText) {
        JLabel newLabel = new JLabel(newText);
        newLabel.setForeground(Color.WHITE);
        newLabel.setFont(new Font("Serif", Font.PLAIN, 12));
        newLabel.setVerticalAlignment(JLabel.TOP);
        newLabel.setHorizontalAlignment(JLabel.CENTER);
        return newLabel;
    }

    JLabel getHighScores(String xmlFile) {
        if (deBug) System.out.println("Getting high scores");
        JLabel viewHighScores = new JLabel();
        String[] highName = new String[5];
        int[] highScore = new int[5];
        // create new save file
        xmlSave = new XML_240();
        // read XML settings
        // XML File saved as
        File xmlCheck = new File(xmlFile);
        if (xmlCheck.exists()) {
            if (deBug) System.out.println("Loading XML file");
            xmlSave.openReaderXML(xmlFile);
            // String object
            highName[0] = (String) xmlSave.ReadObject();
            highScore[0] = (int) xmlSave.ReadObject();
            highName[1] = (String) xmlSave.ReadObject();
            highScore[1] = (int) xmlSave.ReadObject();
            highName[2] = (String) xmlSave.ReadObject();
            highScore[2] = (int) xmlSave.ReadObject();
            highName[3] = (String) xmlSave.ReadObject();
            highScore[3] = (int) xmlSave.ReadObject();
            highName[4] = (String) xmlSave.ReadObject();
            highScore[4] = (int) xmlSave.ReadObject();
            xmlSave.closeReaderXML();
        } else {
            // if there is no high score list, create an empty one
            for (int i = 0; i < 5; i++) {
                highName[i] = "none";
                highScore[i] = 0;
            }
        }
        viewHighScores.setForeground(Color.WHITE);
        viewHighScores.setFont(new Font("Serif", Font.PLAIN, 24));

        viewHighScores.setText("<html>"
                        + "<b>High Scores</b><br>"
                        + "1: " + highName[0] + " scored " + highScore[0] + "<br>"
                        + "2: " + highName[1] + " scored " + highScore[1] + "<br>"
                        + "3: " + highName[2] + " scored " + highScore[2] + "<br>"
                        + "4: " + highName[3] + " scored " + highScore[3] + "<br>"
                        + "5: " + highName[4] + " scored " + highScore[4] + "<br>"
                        + ""
                        + ""
                        + "</html>"
        );
        return viewHighScores;
    }

}