/**
 * Created by Joshua on 11/9/2015.
 * @author Joshua Mathews
 * @version 0.1
 * Filename: Sound.java
 * Short Description: Sound for Final Project
 */

import javax.sound.sampled.*;
import javax.swing.*;
import java.io.IOException;
import java.net.URL;

public class Sound extends JApplet {
    Boolean deBug = false;
    AudioInputStream audioStream;
    Clip audioClip;
    FloatControl volumeControl;
    URL soundFileURL;
    float volumeWidth;
    MainFrame mainFrame;
    Sound(URL newSoundFile) {
        soundFileURL = newSoundFile;
    }

    Sound(MainFrame newMain, URL newSoundFile) {
        mainFrame = newMain;
        soundFileURL = newSoundFile;
    }

    public void init()
    {
        try {
            audioStream = AudioSystem.getAudioInputStream(soundFileURL);
        } catch (UnsupportedAudioFileException e) {
            System.out.println("Not able to load unsupported sound file " + soundFileURL);
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Not able to load sound file " + soundFileURL);
            e.printStackTrace();
        }
        try {
            audioClip = AudioSystem.getClip();
        } catch (LineUnavailableException e) {
            System.out.println("Not able to get line");
            e.printStackTrace();
        }
        try {
            audioClip.open(audioStream);
        } catch (LineUnavailableException e) {
            System.out.println("Not able to get audio stream line");
            e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Not able to open audio stream");
            e.printStackTrace();
        }
        volumeControl = (FloatControl)audioClip.getControl(FloatControl.Type.MASTER_GAIN);
        if (deBug) System.out.println("FC Min = " + volumeControl.getMinimum() + " and Max = " + volumeControl.getMaximum());
        volumeWidth = (volumeControl.getMaximum() - volumeControl.getMinimum());
        //volumeControl.setValue(-80 + (mainFrame.settings.getMasterVolumeRequested() * volumeWidth/ 1000.0f));
//        volumeControl.setValue(-80 + (mainFrame.settings.getSoundVolumeRequested() * volumeWidth/ 1000.0f));
//        volumeControl.setValue(-80 + (mainFrame.settings.getMusicVolumeRequested() * volumeWidth/ 1000.0f));
    }

    void setVolume(int newVol) {
        if (deBug) System.out.println("Received new volume " + newVol);
        volumeControl.setValue(-80 + (newVol * volumeWidth/ 1000.0f));
        if (deBug) System.out.println("Set new volume to " + (-80 + (newVol * volumeWidth/ 1000.0f)));

    }

    void playMusic() {
        if (audioClip != null) {
            audioClip.setMicrosecondPosition(0);
            audioClip.start();
        }
    }
    void stopMusic() {
        if (audioClip != null) {
            audioClip.stop();
        }
    }
    void loopMusic(int newInt) {
        audioClip.loop(newInt);
    }

//    void setVolumeDefault() {
//        volumeControl.setValue(-80 + (1000 * volumeWidth/ 1000.0f));
//    }

}

