/**
* @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: MainFrame.java
 * Short Description: Main Frame for Final Project
*/


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MainFrame extends JFrame implements ActionListener, KeyListener
{
    Boolean deBug = false;
	// class variables
	public MainMenu menu1;
	public InstructionPane instructions;
	public CreditPane credits;
	public SettingsPane settings;
	public NewGamePane newgame;
	public JButton quitGameButton = new JButton("Quit");
    int moveLoop = 0;
    Sound gameMusic;

	public MainFrame ()
	{
		// Create the superclass object for JFrame

		super("Packet Attack");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


        setSize(1280, 720);
        setResizable(false);



        // add key listener to gameBackground
        addKeyListener(this);

        setFocusable(true);

        // create main menu JPanel
        menu1 = new MainMenu();

        // Add main menu as default panel
        add(menu1);

        // create instructions panel
        instructions = new InstructionPane();

        // create credit panel
        credits = new CreditPane();



        // create settings panel
        settings = new SettingsPane(this);

        // Add a listener to each button
        // listen for new game
        menu1.startButton.addActionListener(this);
        // listen for instructions button
        menu1.instructionButton.addActionListener(this);
        // listen for back button on instructions
        instructions.backButton.addActionListener(this);

        // listen for credit button
        menu1.creditButton.addActionListener(this);
        // listen for back button on credits
        credits.backButton.addActionListener(this);

        // listen for settings button
        menu1.settingButton.addActionListener(this);
        // listen for back button on settings
        settings.backButton.addActionListener(this);

        // listen to quit button
        menu1.quitButton.addActionListener(this);

        // Pack the components and make it visible
        pack();
        setVisible(true);

        // Start Music, pass it this frame so we can get default sound values from settings
        if (deBug) System.out.println( "create new Sound object for background music");
        // pass this frame and the path to the sound file
        gameMusic = new Sound(this, (this.getClass().getResource("sounds/CosmicMessages.wav")));
        // initialize the new sound player with an audio file
        gameMusic.init();
        gameMusic.setVolume(settings.getMusicVolumeRequested());
        // tell the new audioClip to loop
        if (deBug) System.out.println("Play the new music");
        gameMusic.loopMusic(100);

    }


    public void actionPerformed(ActionEvent event) {
        Object obj = event.getSource();
        // If the start button is clicked...
        if (obj == menu1.startButton) {
            // make a new game
            // pass settings and quit button
            newgame = new NewGamePane(settings, quitGameButton, this);
            newgame.addKeyListener(this);
            // add action listener to this button
            quitGameButton.addActionListener(this);
            // set the current panel to instructions
            setContentPane(newgame);
            // update the frame to show new panel
            invalidate();
            validate();
        }
        // If the instructions button is clicked...
        if (obj == menu1.instructionButton) {
            // get new high scores
            instructions.updateHighScores();
            // set the current panel to instructions
            setContentPane(instructions);
            // update the frame to show new panel
            invalidate();
            validate();
        }
        // if the credit button is clicked...
        if (obj == menu1.creditButton) {
            // set the current panel to instructions
            setContentPane(credits);
            // update the frame to show new panel
            invalidate();
            validate();
        }

        // if the settings button is clicked...
        if (obj == menu1.settingButton) {
            // set the current panel to instructions
            setContentPane(settings);
            // update the frame to show new panel
            invalidate();
            validate();
        }


		// if the back button on instructions, credits, or settings is hit
		if (obj == instructions.backButton || obj == credits.backButton || obj == settings.backButton || obj == quitGameButton)
		{
			// if settings button, save
            // changed settings first
            newgame = null;
			setContentPane(menu1);
			// update the frame to show new panel
			invalidate();
			validate();
		}
		// if the object is the quit button, get out o' here!
		if (obj == menu1.quitButton)
		{
			System.exit(0);
		}
        if (newgame != null) {
            if (obj == newgame.heroSetButton) {
                newgame.startGameStory();
            }
            if (obj == newgame.femaleHero) {
                newgame.heroChoiceSetup("female", 0);
            }
            if (obj == newgame.maleHero) {
                newgame.heroChoiceSetup("male", 1);
            }
        }
    }
    public void keyPressed(KeyEvent e) {
        // System.out.println("Key: " + e.getKeyCode());
        if (e.getKeyCode() == KeyEvent.VK_SPACE) {
            // Space bar skips loading screen
            if (deBug) System.out.println("Space Bar Hit");
            if (newgame != null) {
                newgame.setSkipLoop(true);
            }
        }

        if (e.getKeyCode() == KeyEvent.VK_1) {
            if (newgame != null) {
                // Attack Monsters
                newgame.heroAttack();
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_2) {
            if (newgame != null) {
                // Attack Monsters
                newgame.heroAttack();
            }
        }

        if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
            if (newgame != null) {
                if (deBug) System.out.println("Right key was pressed");
                Hero hero = newgame.hero;
                if (newgame.getDontMoveRight()) {
                    if (deBug) System.out.println("NOT MOVING HERO RIGHT");
                } else {
                    newgame.hero.setBounds(hero.getX() + hero.getMoveSpeed(), hero.getY(), hero.getWidth(), hero.getHeight());
                }
                if (moveLoop > 8) {
                    moveLoop = 0;
                }
                ImageIcon tempImage = newgame.hero.walkRight[moveLoop];
                newgame.hero.setCurrentSpriteImage(tempImage);
                newgame.hero.setSpriteImage(tempImage);
                moveLoop += 1;
            }
        }
        if (e.getKeyCode() == KeyEvent.VK_LEFT) {
            if (newgame != null) {
                if (deBug) System.out.println("Left key was pressed");
                Hero hero = newgame.hero;
                if (newgame.getLeftInBounds()) {
                    if (deBug) System.out.println("NOT MOVING HERO LEFT");
                } else {
                    newgame.hero.setBounds(hero.getX() - hero.getMoveSpeed(), hero.getY(), hero.getWidth(), hero.getHeight());
                }
                if (moveLoop > 8) {
                    moveLoop = 0;
                }
                ImageIcon tempImage = newgame.hero.walkLeft[moveLoop];
                newgame.hero.setCurrentSpriteImage(tempImage);
                newgame.hero.setSpriteImage(tempImage);
                moveLoop += 1;
            }

        }
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            if (newgame != null) {
                Hero hero = newgame.hero;
                if (newgame.getUpInBounds()) {
                    if (deBug) System.out.println("NOT MOVING HERO UP");
                } else {
                    newgame.hero.setBounds(hero.getX(), hero.getY() - hero.getMoveSpeed(), hero.getWidth(), hero.getHeight());
                }
                if (moveLoop > 8) {
                    moveLoop = 0;
                }
                ImageIcon tempImage = newgame.hero.walkUp[moveLoop];
                newgame.hero.setCurrentSpriteImage(tempImage);
                newgame.hero.setSpriteImage(tempImage);
                moveLoop += 1;

            }
        }

        if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            if (newgame != null) {
                Hero hero = newgame.hero;
                if (newgame.getDownInBounds()) {
                    if (deBug) System.out.println("NOT MOVING HERO DOWN");
                } else {
                    newgame.hero.setBounds(hero.getX(), hero.getY() + hero.getMoveSpeed(), hero.getWidth(), hero.getHeight());
                }
                if (moveLoop > 8) {
                    moveLoop = 0;
                }
                ImageIcon tempImage = newgame.hero.walkDown[moveLoop];
                newgame.hero.setCurrentSpriteImage(tempImage);
                newgame.hero.setSpriteImage(tempImage);
                moveLoop += 1;
            }
        }
    }

    public void keyReleased(KeyEvent e) { //System.out.println("A key was released");
        if (newgame != null) {
            newgame.textOutput.setText("Hero Name: " + newgame.newHeroNameField.getText());
        }
    }

    public void keyTyped(KeyEvent e) {
        //System.out.println("A key was typed");
        // System.out.println("Panel: ");
    }
}
