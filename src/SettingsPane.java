/**
 * @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: SettingsPane.java
 * Short Description: Settings Pane for Final Project
 **/
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;

// XML File saved as
//
//            String difficulty
//            String mapSize
//            String player health bar
//            String monster health bar
//            int level numbers
//            int sound volume
//            int music volume

public class SettingsPane extends JPanel implements ChangeListener, ActionListener
{
    Boolean deBug = false;
    // class parameters
    public JButton backButton, applyButton;
    JLabel headerText, settings, difficulty, mapSize, levels, healthBars, volumeLevels, player, monster, sound, music;
    public JRadioButton difRadioButton_Easy, difRadioButton_Medium, difRadioButton_Hard, setRadioButton,
    mapRadioButton_Small, mapRadioButton_Medium, mapRadioButton_Large,
    pOnRadioButton_On, pOffRadioButton_Off, mOnRadioButton_On, mOffRadioButton_Off;
    public ButtonGroup difficultyBGroup, mapBGroup, playerBGroup, monsterBGroup;
    Box difBox, mapBox, playerBox, monsterBox;
    public JSlider levelSlider, soundSlider, musicSlider;
    int levelNumber=1,defaultLevel = 5,levelRequested = 5;
    int soundVolumeRequested = 1000, musicVolumeRequested = 1000;
    int defaultSoundVolume = 1000, defaultMusicVolume = 1000;
    MainFrame mainFrame;
    XML_240 xmlSave;
    String xmlFile = "PacketAttackSettings.xml";
    // default difficulty setting
    Boolean easyDiff = true;
    Boolean midDiff = false;
    Boolean hardDiff = false;
    // Store difficulty setting as string
    String diffSetting = "Easy";
    // map size variables
    String sizeOfMap = "Small";
    Boolean smallMap = true;
    Boolean midMap = false;
    Boolean largeMap= false;
    // health bars
    String playerHealthBar = "On";
    Boolean heroHealthBarOn = true;
    Boolean heroHealthBarOff = false;
    String monsterHealthBar = "On";
    Boolean monsterHealthBarOn = true;
    Boolean monsterHealthBarOff = false;

    // JButton startButton;
    public SettingsPane(MainFrame newMainFrame) {
        // create new panel from superclass
        super();
        mainFrame = newMainFrame;

        // create local variable for use
        Dimension size;
        // set default text color
        UIManager.put("TitledBorder.titleColor", Color.white);

        // set layout to border for jlabel
        setLayout(new BorderLayout());
        // Create image icon from local file
        ImageIcon backgroundImage = new ImageIcon(this.getClass().getResource("images/Flat_earth_night.jpg"));
        // Add image to JLabel
        JLabel backgroundLabel = new JLabel(backgroundImage);
        // Add new castle image jlabel to menu panel
        add(backgroundLabel);
        // set castle image jlabel layout to Null
        backgroundLabel.setLayout(null);
        // create back button
        backButton = new JButton("Back");
        applyButton = new JButton("Apply");
        applyButton.addActionListener(this);

        // add back button to castle jlabel
        backgroundLabel.add(backButton);
        backgroundLabel.add(applyButton);
        //
        // END DEFAULT WORK
        //

        //
        // START CUSTOM PANEL WORK
        // create settings jlabel
        settings = new JLabel(
                //Title of the jlabel
                "<html><b>Settings for Packet Attack</b></html>"
        );
        settings.setOpaque(false);
        settings.setBorder(new EmptyBorder(0,10,10,10));
        // Sets jlabel font color
        settings.setForeground(Color.WHITE);
        // Sets the font style and size
        settings.setFont(new Font("Serif", Font.ITALIC, 30));
        // Add settings jlabel to castle jlabel
        backgroundLabel.add(settings);
        // get preferred size of settings label
        size = settings.getPreferredSize();
        // set size and placement of settings label- (starting x, starting y, x width, y width)
        settings.setBounds(640 - (size.width / 2), 30, size.width, size.height);

        // Create header text for settings panel
        difficulty = settingsHeaderText("<html><b>Difficulty</b></html>");
        mapSize = settingsHeaderText("<html><b>Map Size</b></html>");
        levels = settingsHeaderText("<html><b>Levels</b></html>");
        healthBars = settingsHeaderText("<html><b>Health Bars</b></html>");
        volumeLevels = settingsHeaderText("<html><b>Volume Controls</b></html>");
        player = settingsHeaderText("<html><b>Players</b></html>");
        player.setFont(new Font("Serif", Font.ITALIC, 15));
        monster = settingsHeaderText("<html><b>Monster</b></html>");
        monster.setFont(new Font("Serif", Font.ITALIC, 15));
        sound = settingsHeaderText("<html><b>Sound</b></html>");
        sound.setFont(new Font("Serif", Font.ITALIC, 15));
        music = settingsHeaderText("<html><b>Music</b></html>");
        music.setFont(new Font("Serif", Font.ITALIC, 15));


        // Add header to the castle image
        backgroundLabel.add(difficulty);
        backgroundLabel.add(mapSize);
        backgroundLabel.add(levels);
        backgroundLabel.add(healthBars);
        backgroundLabel.add(volumeLevels);
        backgroundLabel.add(player);
        backgroundLabel.add(monster);
        backgroundLabel.add(sound);
        backgroundLabel.add(music);


        // Set size and location of header text
        size = difficulty.getPreferredSize();
        difficulty.setBounds(200 - (size.width / 2), 100, size.width, size.height);
        size = mapSize.getPreferredSize();
        mapSize.setBounds(600 - (size.width / 2), 100, size.width, size.height);
        size = levels.getPreferredSize();
        levels.setBounds(1000 - (size.width / 2), 100, size.width, size.height);
        size = healthBars.getPreferredSize();
        healthBars.setBounds(400 - (size.width / 2), 300, size.width, size.height);
        size = volumeLevels.getPreferredSize();
        volumeLevels.setBounds(820 - (size.width / 2), 300, size.width, size.height);
        size = player.getPreferredSize();
        player.setBounds(350 - (size.width / 2), 350, size.width, size.height);
        size = monster.getPreferredSize();
        monster.setBounds(350 - (size.width / 2), 400, size.width, size.height);
        size = sound.getPreferredSize();
        sound.setBounds(750 - (size.width / 2), 340, size.width, size.height);
        size = music.getPreferredSize();
        music.setBounds(750 - (size.width / 2), 390, size.width, size.height);

        // create new save file
        xmlSave = new XML_240();
        // read XML settings
        // XML File saved as
        File xmlCheck = new File(xmlFile);
        if (xmlCheck.exists()) {
            if (deBug) System.out.println("Loading XML file");
            xmlSave.openReaderXML(xmlFile);

            // String difficulty
            diffSetting = (String) xmlSave.ReadObject();
            if (deBug) System.out.println("Loading difficulty " + diffSetting);
            switch (diffSetting) {
                case "Easy":
                    easyDiff = true;
                    midDiff = false;
                    hardDiff = false;
                    break;
                case "Medium":
                    midDiff = true;
                    easyDiff = false;
                    hardDiff = false;
                    break;
                case "Hard":
                    hardDiff = true;
                    easyDiff = false;
                    midDiff = false;
                    break;
            }

            // String mapSize
            sizeOfMap = (String) xmlSave.ReadObject();
            if (deBug) System.out.println("Loading map size " + sizeOfMap);
            switch (sizeOfMap) {
                case "Small":
                    smallMap = true;
                    midMap = false;
                    largeMap = false;
                    break;
                case "Medium":
                    midMap = true;
                    smallMap = false;
                    largeMap = false;
                    break;
                case "Large":
                    largeMap = true;
                    smallMap = false;
                    midMap = false;
                    break;
            }

            // String player health bar
            playerHealthBar = (String) xmlSave.ReadObject();
            if (deBug) System.out.println("Loading player health bar " + playerHealthBar);
            switch (playerHealthBar) {
                case "On":
                    heroHealthBarOn = true;
                    heroHealthBarOff = false;
                    break;
                case "Off":
                    heroHealthBarOff = true;
                    heroHealthBarOn = false;
                    break;
            }
            // String monster health bar
            monsterHealthBar = (String) xmlSave.ReadObject();
            if (deBug) System.out.println("Loading monster health bar " + monsterHealthBar);
            switch (monsterHealthBar) {
                case "On":
                    monsterHealthBarOn = true;
                    monsterHealthBarOff = false;
                    break;
                case "Off":
                    monsterHealthBarOff = true;
                    monsterHealthBarOn = false;
                    break;
            }
            // int level numbers
            levelRequested = (int) xmlSave.ReadObject();
            if (deBug) System.out.println("Loading levels requested " + levelRequested);

            // int master volume
            //masterVolumeRequested = (int) xmlSave.ReadObject();
            //if (deBug) System.out.println("Loading master volume " + masterVolumeRequested);

            // int sound volume
            soundVolumeRequested = (int) xmlSave.ReadObject();
            if (deBug) System.out.println("Loading sound volume " + soundVolumeRequested);

            // int music volume
            musicVolumeRequested = (int) xmlSave.ReadObject();
            if (deBug) System.out.println("Loading music volume " + musicVolumeRequested);

            xmlSave.closeReaderXML();
        }

        // ===================================
        // create new buttons using custom method
        //Difficulty Buttons
        difRadioButton_Easy = defaultSettingsRadioButton("Easy", easyDiff, "This is easy mode");
        difRadioButton_Medium = defaultSettingsRadioButton("Medium", midDiff, "A little more difficult");
        difRadioButton_Hard = defaultSettingsRadioButton("Hard", hardDiff, "Death is certain");

        //Map Size Buttons
        mapRadioButton_Small = defaultSettingsRadioButton("Small", smallMap, "The right amount of space");
        mapRadioButton_Medium = defaultSettingsRadioButton("Medium", midMap, "A little larger, you'll be OK");
        mapRadioButton_Large = defaultSettingsRadioButton("Large", largeMap, "Don't get lost inside the terminal");

        //Health Bar Buttons
        pOnRadioButton_On = defaultSettingsRadioButton("On", heroHealthBarOn, "Show health bars");
        pOffRadioButton_Off = defaultSettingsRadioButton("Off", heroHealthBarOff, "Hide health bars");

        mOnRadioButton_On = defaultSettingsRadioButton("On", monsterHealthBarOn, "Show monsters health bars");
        mOffRadioButton_Off = defaultSettingsRadioButton("Off", monsterHealthBarOff, "Hide monster health bars. You're not scared.");



        // ===================================
        // Add similar buttons into ButtonGroup
        difficultyBGroup = new ButtonGroup();
        difficultyBGroup.add(difRadioButton_Easy);
        difficultyBGroup.add(difRadioButton_Medium);
        difficultyBGroup.add(difRadioButton_Hard);

        mapBGroup = new ButtonGroup();
        mapBGroup.add(mapRadioButton_Small);
        mapBGroup.add(mapRadioButton_Medium);
        mapBGroup.add(mapRadioButton_Large);

        playerBGroup = new ButtonGroup();
        playerBGroup.add(pOnRadioButton_On);
        playerBGroup.add(pOffRadioButton_Off);

        monsterBGroup = new ButtonGroup();
        monsterBGroup.add(mOnRadioButton_On);
        monsterBGroup.add(mOffRadioButton_Off);



        // ===================================
        // Add each button from ButtonGroup to a new Box
        difBox = Box.createVerticalBox();
        difBox.add(difRadioButton_Easy);
        difBox.add(difRadioButton_Medium);
        difBox.add(difRadioButton_Hard);

        mapBox = Box.createVerticalBox();
        mapBox.add(mapRadioButton_Small);
        mapBox.add(mapRadioButton_Medium);
        mapBox.add(mapRadioButton_Large);

        playerBox = Box.createHorizontalBox();
        playerBox.add(pOnRadioButton_On);
        playerBox.add(pOffRadioButton_Off);

        monsterBox = Box.createHorizontalBox();
        monsterBox.add(mOnRadioButton_On);
        monsterBox.add(mOffRadioButton_Off);



        if (levelRequested != defaultLevel) {
            defaultLevel = levelRequested;
        }

        // set level slider to use white text and times new roman
        levelSlider = new JSlider(JSlider.HORIZONTAL, 0, 50, defaultLevel);
        levelSlider.setBorder(BorderFactory.createTitledBorder("You have selected " + levelRequested + "  level(s)."));
        levelSlider.setMajorTickSpacing(10);
        levelSlider.setMinorTickSpacing(5);
        levelSlider.setForeground(Color.white);
        levelSlider.setPaintTicks(true);
        levelSlider.setPaintLabels(true);
        levelSlider.setOpaque(false);
        levelSlider.addChangeListener(this);

        //if (masterVolumeRequested != defaultMasterVolumeLevel) {
            //defaultMasterVolumeLevel = masterVolumeRequested;
        //}
        //masterVolumeSlider = new JSlider(JSlider.HORIZONTAL, 0, 1000, defaultMasterVolumeLevel);
        //masterVolumeSlider.addChangeListener(this);
        //masterVolumeSlider.setPaintTicks(true);
        //masterVolumeSlider.setPaintLabels(true);
        //masterVolumeSlider.setOpaque(false);
        //masterVolumeSlider.setBounds(600, 344, 200, 20);
        //if (soundVolumeRequested != defaultSoundVolume) {
            //defaultSoundVolume = soundVolumeRequested;
        //}
        soundSlider = new JSlider(JSlider.HORIZONTAL, 0, 1000, defaultSoundVolume);
        soundSlider.addChangeListener(this);
        soundSlider.setPaintTicks(true);
        soundSlider.setPaintLabels(true);
        soundSlider.setOpaque(false);
        soundSlider.setBounds(780, 355, 120, 20);
        if (musicVolumeRequested != defaultMusicVolume) {
            defaultMusicVolume = musicVolumeRequested;
        }
        musicSlider = new JSlider(JSlider.HORIZONTAL, 0, 1000, defaultMusicVolume);
        musicSlider.addChangeListener(this);
        musicSlider.setPaintTicks(true);
        musicSlider.setPaintLabels(true);
        musicSlider.setOpaque(false);
        musicSlider.setBounds(780, 405, 120, 20);


        // set defaults for buttons
        //diffSetting = difficultyBGroup.getSelection().getActionCommand();

        // ===================================
        // Adding ButtonGroup Box to castle background
        // Add new difficulty box to the castle background
        backgroundLabel.add(difBox);
        backgroundLabel.add(mapBox);
        backgroundLabel.add(levelSlider);
        backgroundLabel.add(playerBox);
        backgroundLabel.add(monsterBox);
        //backgroundLabel.add(masterVolumeSlider);
        backgroundLabel.add(soundSlider);
        backgroundLabel.add(musicSlider);
        // set size and placement of new button box
        size = difBox.getPreferredSize();
        difBox.setBounds(210 - (size.width / 2), 150, size.width, size.height);
        size = mapBox.getPreferredSize();
        mapBox.setBounds(610 - (size.width / 2), 150, size.width, size.height);
        size = levelSlider.getPreferredSize();
        levelSlider.setBounds(1000 - (size.width / 2), 150, size.width, size.height);
        size = playerBox.getPreferredSize();
        playerBox.setBounds(440 - (size.width / 2), 350, size.width, size.height);
        size = monsterBox.getPreferredSize();
        monsterBox.setBounds(440 - (size.width / 2), 400, size.width, size.height);


        // ===================================
        // Apply and Back buttons
        // get prefered size of apply button
        size = applyButton.getPreferredSize();
        // set size and placement of back button- (starting x, starting y, x width, y width)
        applyButton.setBounds(700 - (size.width / 2), 550, size.width, size.height);
        // get prefered size of apply button
        size = backButton.getPreferredSize();
        // set size and placement of back button- (starting x, starting y, x width, y width)
        backButton.setBounds(500 - (size.width / 2), 550, size.width, size.height);



    }
    // Method to create radio button
    JRadioButton defaultSettingsRadioButton(String buttonText, Boolean defaultButton, String buttonToolTip)
    {
        setRadioButton = new JRadioButton(buttonText);
        setRadioButton.setOpaque(false);
        setRadioButton.setBorder(new EmptyBorder(5, 5, 5, 5));
        setRadioButton.setForeground(Color.WHITE);
        setRadioButton.setFont(new Font("Serif", Font.ITALIC, 15));
        setRadioButton.setToolTipText(buttonToolTip);
        if (defaultButton)
        {
            setRadioButton.setMnemonic(KeyEvent.VK_C);
            setRadioButton.setSelected(true);
        }
        setRadioButton.setActionCommand(buttonText);
        return setRadioButton;
    }
    // Method to create settings header text
    JLabel settingsHeaderText(String labelText)
    {
        headerText = new JLabel(labelText);
        headerText.setOpaque(false);
        headerText.setBorder(new EmptyBorder(5, 5, 5, 5));
        headerText.setForeground(Color.WHITE);
        headerText.setFont(new Font("Serif", Font.ITALIC, 18));
        return headerText;
    }

    // Watch level slider and update label
    public void stateChanged(ChangeEvent e) {
        Object obj = e.getSource();
        if (obj == levelSlider) {
            levelNumber = levelSlider.getValue();
            // if the user selects 0 levels, reset it to the minimum value 1
            if (levelNumber < 1) {
                levelSlider.setValue(1);
                levelNumber = levelSlider.getValue();
            }
            levelSlider.setBorder(BorderFactory.createTitledBorder("You have selected " + levelNumber + " levels."));
        }
        //if (obj == masterVolumeSlider) {
            //masterVolumeRequested = masterVolumeSlider.getValue();
            //System.out.println("New volume selected " + masterVolumeRequested + ". Setting master volume.");
            //mainFrame.gameMusic.setVolume(masterVolumeRequested);
        //}
        if (obj == soundSlider) {
            soundVolumeRequested = soundSlider.getValue();
            System.out.println("New sound selected " + soundVolumeRequested + ". Setting sound volume.");
            // new game sounds request this value
            // game sounds are created when a new game is started, so they can ask for the correct value
            Sound testSound = new Sound((this.getClass().getResource("sounds/swordhit.wav")));
            testSound.init();
            testSound.setVolume(soundVolumeRequested);
            testSound.playMusic();

        }
        if (obj == musicSlider) {
            musicVolumeRequested = musicSlider.getValue();
            if (deBug) System.out.println("New music selected " + musicVolumeRequested + ". Setting music volume.");
            // Tell main frame to set the game music to requested volume.
            // Game music does not pick up changes in settings after launched
            mainFrame.gameMusic.setVolume(musicVolumeRequested);
        }
    }

    // Listen for back and apply buttons
    public void actionPerformed(ActionEvent event) {
        Object obj = event.getSource();
        // If the start button is clicked...
        if (obj == applyButton) {
            // get values for settings & set in global variables

            xmlSave.openWriterXML(xmlFile);

            // print out current selected button.
            // Difficulty String:
            diffSetting = difficultyBGroup.getSelection().getActionCommand();
            if (deBug) System.out.println("Difficulty: " + diffSetting);
            xmlSave.writeObject(diffSetting);
            // trying to set the button group to default the new selection
            //difficultyBGroup.setSelected(difficultyBGroup.getSelection(), true);

            // Map Size String:
            sizeOfMap = mapBGroup.getSelection().getActionCommand();
            if (deBug) System.out.println("Map Size: " + sizeOfMap);
            xmlSave.writeObject(sizeOfMap);

            // Player Health Bars String:
            playerHealthBar = playerBGroup.getSelection().getActionCommand();
            if (deBug) System.out.println("Player Health Bars: " + playerHealthBar);
            xmlSave.writeObject(playerHealthBar);

            // Monster Health Bars String:
            monsterHealthBar = monsterBGroup.getSelection().getActionCommand();
            if (deBug) System.out.println("Monster Health Bars: " + monsterHealthBar);
            xmlSave.writeObject(monsterHealthBar);

            // Number of Levels
            int levelNumbers = levelSlider.getValue();
            if (deBug) System.out.println("Number of Level: " + levelNumbers);
            xmlSave.writeObject(levelNumbers);

            // Master Volume
            //int masterVolumeRequested = masterVolumeSlider.getValue();
            //if (deBug) System.out.println("Master Volume: " + masterVolumeRequested);
            //xmlSave.writeObject(masterVolumeRequested);

            // Sound Volume
            int soundVolume = soundSlider.getValue();
            if (deBug) System.out.println("Sound Volume: " + soundVolume);
            xmlSave.writeObject(soundVolume);

            // Music Volume
            int musicVolume = musicSlider.getValue();
            if (deBug) System.out.println("Music Volume: " + musicVolume);
            xmlSave.writeObject(musicVolume);

            // save file
            xmlSave.closeWriterXML();
            // if settings button, save changed settings first
            mainFrame.setContentPane(mainFrame.menu1);
            // update the frame to show new panel
            mainFrame.invalidate();
            mainFrame.validate();
        }
    }
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
//        …. you write your code here ….
//        code that draw, paint, shows whatever
//        needs to be shown when the screen is repainted
    }

    boolean getHeroHealthBarVisable() {
        return playerBGroup.getSelection().getActionCommand().equals("On");
    }
    boolean getMonsterHealthBarVisable() {
        return monsterBGroup.getSelection().getActionCommand().equals("On");
    }

    String getDiffSetting() {
        return diffSetting;
    }

    int getNumberOfLevels() {
        return levelSlider.getValue();
    }

    //int getMasterVolumeRequested() {
        //return masterVolumeRequested;
    //}

    int getSoundVolumeRequested() {
        return soundVolumeRequested;
    }

    int getMusicVolumeRequested() {
        return musicVolumeRequested;
    }

    String getMapSize() {
        return sizeOfMap;
    }
}

/*
* Helpful Links:
* Box and Radio Buttons
* http://www.java2s.com/Tutorial/Java/0240__Swing/UseGridBagLayouttolayoutRadioButtons.htm
*
*
*/