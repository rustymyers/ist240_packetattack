/**
 * @author Rusty Myers, Joshua Mathews
 * @version 1.0
 * Filename: App.java
 * Short Description: App for Final Project
**/
public class App
{
	public static void main(String args[]) {
        // Create main JFrame
        new MainFrame();
    }
}

/*
* Notes:
* Generating Levels:
* http://www.roguebasin.com/index.php?title=Basic_BSP_Dungeon_generation
* http://gamedevelopment.tutsplus.com/tutorials/create-a-procedurally-generated-dungeon-cave-system--gamedev-10099
* http://gamedevelopment.tutsplus.com/tutorials/cave-levels-cellular-automata--gamedev-9664
* http://www.roguebasin.com/index.php?title=Dungeon-Building_Algorithm
* Apple Dev Info for Dock Icon
* https://developer.apple.com/library/prerelease/mac/documentation/Java/Conceptual/Java14Development/07-NativePlatformIntegration/NativePlatformIntegration.html#//apple_ref/doc/uid/TP40001909-SW1
* Apple Dev Info for Full Screen
* https://developer.apple.com/library/prerelease/mac/documentation/Java/Reference/Java_PropertiesRef/Articles/JavaSystemProperties.html#//apple_ref/doc/uid/TP40008047
*
*/

/*
* Credits:
* Earch Composite
* 	https://en.wikipedia.org/wiki/Ecological_light_pollution
* Dr. Evil Packet
* 	https://pixabay.com/en/users/flegmatik95-1054817/
* Sprite Generation
*   http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/
* Network Image Original
*   Martin Grandjean - Own work : http://www.martingrandjean.ch/wp-content/uploads/2013/10/Graphe3.png
* Network Port
*   https://pixabay.com/en/ethernet-port-switch-computer-160832/
* Marble Texture
*   http://www.texturearchive.com/Display_2.aspx?sub1=Stone&sub2=Marble&sub3=null
* Space Texture
*   https://commons.wikimedia.org/wiki/File:Polycyclic_Aromatic_Hydrocarbons_In_Space.jpg
* Stairs Image
*   http://greth95.deviantart.com/art/Mack-s-Tile-Set-D-179904805
* Health Bottle
*   https://pixabay.com/en/chemistry-lab-experiment-science-306982/
* Speed Bolt
*   https://pixabay.com/en/lightning-energy-bolt-green-circle-305038/
* Circuit Board Background
*   https://pixabay.com/en/board-computer-chip-data-processing-564813/
* Backlight Circuit board
*   https://www.flickr.com/photos/derekgavey/5528275910
* WiFi Symbol
*   https://pixabay.com/en/wifi-wireless-internet-icons-798314/
* Sounds
*   http://soundbible.com/1823-Winning-Triumphal-Fanfare.html
*   http://soundfxnow.com/sound-fx/power-down/
*   http://www.dl-sounds.com/royalty-free/cosmic-messages/
*   http://themushroomkingdom.net/sounds/wav/smw/smw_door_opens.wav
*   http://themushroomkingdom.net/sounds/wav/smw/smw_coin.wav
*   http://themushroomkingdom.net/sounds/wav/smw/smw_1-up.wav
*   http://soundbible.com/1001-Heartbeat.html
*   http://soundbible.com/706-Swoosh-3.html
*   http://soundbible.com/1540-Computer-Error-Alert.html
*   http://soundbible.com/857-Swords-Clashing.html
*   http://soundbible.com/1780-Bow-Fire-Arrow.html
*
*
* */