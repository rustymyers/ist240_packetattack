/**
 * @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: Hero.java
 * Short Description: Hero Class for Final Project
 **/
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Hero extends Sprite {
    // store hero image
    BufferedImage heroImages;
    // points gained
    int heroPoints = 0;

    // create new hero with name and sex. 0 is female, 1 is male
    Hero(String newName, int model) {
        super(newName, 100);
        // get new hero sprite page from images
        try {
            if (model == 0) {
                heroImages = ImageIO.read(this.getClass().getResource("images/femaleHero2.png"));
                // setting weapon type to bow
                setWeaponType(2);
            } else {
                heroImages = ImageIO.read(this.getClass().getResource("images/maleHero2.png"));
                // setting weapon type to stick
                setWeaponType(1);
            }
        } catch (IOException e) {
            System.out.println("female or male hero images missing");
        } catch (IllegalArgumentException e) {
            System.out.println("female or male hero images missing\n" + e);
        }
        // make button 34 wide and 55 high, size of each sprite image
        setBounds(0, 0, 64, 64);
        // create a new sprite with name Hero and health of 100
        setImages(heroImages,1);
        // set move speed to 10
        setMoveSpeed(10);
        // set attack speed to 99
        setAttackSpeed(99);

    }

    void increaseHeroPoints(int increaseInt) {
        heroPoints += increaseInt;
    }

    int getHeroPoints() {
        return heroPoints;
    }

}
