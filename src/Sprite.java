/**
 * @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: Sprite.java
 * Short Description: Superclass Sprite for Heroes and Monsters for Final Project
 * Date: 10/15/15
 **/

import javax.swing.*;
import java.awt.image.BufferedImage;

public class Sprite extends JButton {
    Boolean deBug = false;
    int health, moveSpeed, maxHealth, defaultMoveSpeed, attackSpeed;
    int weaponType = 1;
    String name;
    ImageIcon spriteImage, defaultSpriteImage, currentSpriteImage;
    // store full image of hero
    BufferedImage spriteImages;
    // create images for walking down
    ImageIcon[] walkDown;
    // create images for walking up
    ImageIcon[] walkUp;
    // create images for walking left
    ImageIcon[] walkLeft;
    // create images for walking right
    ImageIcon[] walkRight;
    // create images for attack1
    ImageIcon[] attack1;
    // create images for attack2
    ImageIcon[] attack2;
    // counter for attack icon
    int attack1counter;
    // counter for attack icon
    int attack2counter;
    // damage per attack
    int attackDamage = 5;

    // construct new sprite image
    Sprite(String newName, int newHealth) {
        // set health from received value
        health = newHealth;
        maxHealth = newHealth;
        // set name from received value
        name = newName;
        // set speed
        defaultMoveSpeed = 10;
        setMoveSpeed(defaultMoveSpeed);
        // set opaque to false
        setOpaque(false);


        // set button background to false unless debugging
        if (deBug) {
            // print debug is on
            System.out.println("Debug On");
            // set border to painted
            setBorderPainted(true);
        } else {
            // make border invisible
            setBorderPainted(false);
            setContentAreaFilled(false);
        }

    }

    void setWeaponType(int newWeaponType) {
        weaponType = newWeaponType;
    }

    // Create images for sprite
    // Pass BufferedImage, Sprite Type: 1 = SpriteSheet, 2 = ?
    void setImages(BufferedImage newImage, int spriteType) {

        spriteImages = newImage;


        if (spriteType == 1) {

            walkDown = new ImageIcon[9];
            // create images for walking up
            walkUp = new ImageIcon[9];
            // create images for walking left
            walkLeft = new ImageIcon[9];
            // create images for walking right
            walkRight = new ImageIcon[9];
            if (weaponType == 1) {
                // create images for attack1
                attack1= new ImageIcon[8];
                // create images for attack2
                attack2= new ImageIcon[8];
                // counter for attack icon
                attack1counter = 0;
                // counter for attack icon
                attack2counter = 0;
            } else if (weaponType == 2) {
                // create images for attack1
                attack1= new ImageIcon[12];
                // create images for attack2
                attack2= new ImageIcon[12];
                // counter for attack icon
                attack1counter = 0;
                // counter for attack icon
                attack2counter = 0;
            } else if (weaponType == 3) {
                // create images for attack1
                attack1= new ImageIcon[5];
                // create images for attack2
                attack2= new ImageIcon[5];
                // counter for attack icon
                attack1counter = 0;
                // counter for attack icon
                attack2counter = 0;
            }

            // set size of sprite
            setBounds(0, 0, 64, 64);

            // This is a SpriteSheet http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/
            // Get starting, facing image for hero
            spriteImage = new ImageIcon(spriteImages.getSubimage(0, 128, getWidth(), getHeight()));
            setDefaultSpriteImage(spriteImage);
            setCurrentSpriteImage(spriteImage);
            // set button to new image
            setIcon(spriteImage);

            updateSpriteWalkImages(spriteImages);
            // Generate Images for Sprite Weapon: 1 = sword, 2 = bow
            updateSpriteFightImages(spriteImages, weaponType);

        }
        if (spriteType == 2) {
            // this is a different type of image
            // Get starting, facing image for hero
            spriteImage = new ImageIcon(spriteImages.getSubimage(15, 650, getWidth(), getHeight()));
            // set button to new image
            setIcon(spriteImage);

//            updateWalkImages(spriteImages);
//            updateFightImages(spriteImages,weaponType);
        }

    }
    // set the image that is currently displayed
    void setSpriteImage(ImageIcon newSpriteImage) {
        setIcon(newSpriteImage);
    }

    // set default image
    void setDefaultSpriteImage(ImageIcon newSpriteImage) {
        defaultSpriteImage = newSpriteImage;
    }
    // set current image
    void setCurrentSpriteImage(ImageIcon newSpriteImage) {
        currentSpriteImage = newSpriteImage;
    }

    ImageIcon getCurrentSpriteImage() {
        return currentSpriteImage;
    }

    void returnToDefaultImage() {
        setSpriteImage(spriteImage);
    }
    // update arrays of images for walking
    void updateSpriteWalkImages(BufferedImage newspriteImages) {

        // sprite images are 0 apart on x
        int spriteImageSpace = 0;
        // sprite images start 1 in on x
        int spriteImageStart = 0;
        // Sprites image width
        int spriteImageWidth = 64;
        // Sprites image height
        int spriteImageHeight = 64;
        // for each image in the series, create walking images
        int spriteImageY = 640;
        // create array of images for walking down
        for (int i = 0; i < walkDown.length; i++) {
            ImageIcon newImage = new ImageIcon(newspriteImages.getSubimage(spriteImageStart, spriteImageY, spriteImageWidth, spriteImageHeight));
            if (i == 0) {
                spriteImageStart += spriteImageSpace + spriteImageWidth;
            } else {
                spriteImageStart += spriteImageSpace + spriteImageWidth;
            }
            walkDown[i] = newImage;
        }
        // create array of images for walking up
        // reset start position
        spriteImageStart = 0;
        spriteImageY = 512;
        for (int i = 0; i < walkUp.length; i++) {
            ImageIcon newImage = new ImageIcon(newspriteImages.getSubimage(spriteImageStart, spriteImageY, spriteImageWidth, spriteImageHeight));
            if (i == 0) {
                spriteImageStart += spriteImageSpace + spriteImageWidth;
            } else {
                spriteImageStart += spriteImageSpace + spriteImageWidth;
            }
            walkUp[i] = newImage;
        }
        // create array of images for walking right
        // reset start position
        spriteImageStart = 0;
        spriteImageY = 704;
        for (int i = 0; i < walkRight.length; i++) {
            ImageIcon newImage = new ImageIcon(newspriteImages.getSubimage(spriteImageStart, spriteImageY, spriteImageWidth, spriteImageHeight));
            if (i == 0) {
                spriteImageStart += spriteImageSpace + spriteImageWidth;
            } else {
                spriteImageStart += spriteImageSpace + spriteImageWidth;
            }
            walkRight[i] = newImage;
        }
        // create array of images for walking left
        // reset start position
        spriteImageStart = 0;
        spriteImageY = 578;
        for (int i = 0; i < walkLeft.length; i++) {
            ImageIcon newImage = new ImageIcon(newspriteImages.getSubimage(spriteImageStart, spriteImageY, spriteImageWidth, spriteImageHeight));
            if (i == 0) {
                spriteImageStart += spriteImageSpace + spriteImageWidth;
            } else {
                spriteImageStart += spriteImageSpace + spriteImageWidth;
            }
            walkLeft[i] = newImage;
        }
    }
    // update arrays of images for attacking
    void updateSpriteFightImages(BufferedImage newspriteImages, int newWeaponType) {
        // create array of images for fighting
        // sprite images are 30 apart on x
        int spriteImageSpace;
        // sprite images start 15 in on x
        int spriteImageStart = 0;

        // set width
        int spriteImageWidth;
        if (newWeaponType == 1) {
            spriteImageWidth = 64;
            spriteImageSpace = 0;
        } else if (newWeaponType == 2) {
            spriteImageWidth = 64;
            spriteImageSpace = 0;
        } else if (newWeaponType == 3) {
            spriteImageWidth = 64;
            spriteImageSpace = 0;
        } else {
            spriteImageWidth = 0;
            spriteImageSpace = 0;
        }

        // set height
        int spriteImageHeight;
        if (newWeaponType == 1) {
            spriteImageHeight = 64;
        } else if (newWeaponType == 2) {
            spriteImageHeight = 64;
        } else if (newWeaponType == 3) {
            spriteImageHeight = 64;
        } else {
            spriteImageHeight = 0;
        }

        // set start point
        int spriteImageY;
        if (newWeaponType == 1) {
            spriteImageY = 322;
        } else if (newWeaponType == 2) {
            spriteImageY = 1090;
        } else if (newWeaponType == 3) {
            spriteImageY = 832;
        } else {
            spriteImageY = 0;
        }

        // set start of image
        // for each image in the series, create attack 1
        // create array of images for walking down
        for (int i = 0; i < attack1.length; i++) {
            ImageIcon newImage = new ImageIcon(newspriteImages.getSubimage(spriteImageStart, spriteImageY, spriteImageWidth, spriteImageHeight));
            if (i == 0) {
                spriteImageStart += spriteImageSpace + spriteImageWidth;
            } else {
                spriteImageStart += spriteImageSpace + spriteImageWidth;
            }
            attack1[i] = newImage;
        }
        // create array of images for attack 2
        // reset start position
        spriteImageStart = 0;
        if (newWeaponType == 1) {
            spriteImageY = 448;
        } else if (newWeaponType == 2) {
            spriteImageY = 1216;
        } else if (newWeaponType == 3) {
            spriteImageY = 960;
        }
        for (int i = 0; i < attack2.length; i++) {
            // try { heroImages = ImageIO.read(new File("images/femaleHero.png")); } catch (IOException e) {};
            // Get facing walking image for hero - walking down starts at 15X, 525Y
            ImageIcon newImage;
            newImage = new ImageIcon(newspriteImages.getSubimage(spriteImageStart, spriteImageY, spriteImageWidth, spriteImageHeight));
            if (i == 0) {
                spriteImageStart += spriteImageWidth;
            } else {
                spriteImageStart +=  spriteImageSpace + spriteImageWidth;
            }
            attack2[i] = newImage;

        }
    }
    // Update name of sprite
    void setSpriteName(String newName) {
        name = newName;
    }
    String getSpriteName() {
        return name;
    }

    // make sprite attack
    Boolean attackLeft() {
        if (deBug) System.out.println("Doing attack one. counter: " + attack1counter);
        if (attack1counter > attack1.length-1) {
            attack1counter = 0;
        }
        setIcon(attack1[attack1counter]);
        attack1counter++;
        // Only do damage to hero when Monster reaches end of attack animation
        return attack1counter == attack1.length;
    }
    // make sprite attack
    Boolean attackRight() {
        if (deBug) System.out.println("Doing attack two. counter: " + attack2counter);
        if (attack2counter > attack2.length-1) {
            attack2counter = 0;
        }
        setIcon(attack2[attack2counter]);
        attack2counter++;
        // Only do damage to hero when Monster reaches end of attack animation
        return attack2counter == attack2.length;
    }
    // return health of sprite
    int getHealth() {
        return health;
    }

    int getMaxHealth() { return maxHealth; }

    void setMaxHealth(int newHealth) { maxHealth = newHealth; }

    // reduce sprite health
    int reduceHealth(int negativeHealth) {
        health -= negativeHealth;
        return health;
    }

    // increase sprite health
    void increaseHealth(int posotiveHealth) {
        // set new health to current health plus new addition
        int newHealth = health + posotiveHealth;
        // if new health is greater than previous max, increase max
        if (newHealth > getMaxHealth()) {
            setMaxHealth(newHealth);
        }
        health = newHealth;
    }

    // set the speed of the sprite
    void setMoveSpeed(int newSpeed) {moveSpeed = newSpeed;}

    // get the speed of the sprite
    int getMoveSpeed() {
        return moveSpeed;
    }

    // set the speed of the sprite
    void setAttackSpeed(int newSpeed) {attackSpeed = newSpeed;}

    // get the speed of the sprite
    int getAttackSpeed() {
        return attackSpeed;
    }

    void increaseAttackSpeed(int newSpeed) {
        attackSpeed -= newSpeed;
    }

    // Create and return a random int. Pass a range to it
    int makeRandomInt(double range)
    {
        // create random value
        double r = Math.random();
        // create new value
        int myrandomNumber;
        // Create range between 0 and 5
        myrandomNumber = (int)(r*range);
        // Return value
        return myrandomNumber;
    }
    int getAttackDamage() {return attackDamage;}
    void increaseAttackDamage(int increaseDamage) {attackDamage += increaseDamage;}

    int getWeaponType () {return weaponType;}
//
//    void setAttackDamage(int newAttackDamage) {attackDamage = newAttackDamage;}

    // set health of sprite
//    void setHealth(int newHealth) {
//        health = newHealth;
//    }

//    void increaseMoveSpeed(int newSpeed) {
//        moveSpeed += newSpeed;
//    }


}
