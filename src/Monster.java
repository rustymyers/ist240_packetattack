/**
 * @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: Monster.java
 * Short Description: Monster Class for Final Project
 * Date: 10/15/15
 * DrEvil created: http://gaurav.munjal.us/Universal-LPC-Spritesheet-Character-Generator/#?body=skeleton&arms=plate&greaves=metal&armor=chest_plate&hat=hood_cloth&shoes=boots_metal&weapon=dagger&eyes=red
 *
 **/

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Monster extends Sprite {
    Boolean deBug = false;
    BufferedImage monsterImages;
    String[] monsterName = new String[] {"Firmware Bug","Virus","Firewall"};


    Monster(String newName, int newHealth, Boolean DrEvil) {
        super(newName,newHealth);
        // make button 34 wide and 55 high, size of each sprite image
        setBounds(0, 0, 64, 64);

        int randomChoice;
        if (DrEvil) {
            if (deBug) System.out.println("Creating Dr Evil");
            randomChoice = 3;
            // set name
            setSpriteName("Dr. Evil Packet");
        } else {
            if (deBug) System.out.println("Creating Random Monster");
            randomChoice = makeRandomInt(3);
            // set name
            setSpriteName(monsterName[randomChoice]);
        }
        // create random monster
        generateMonster(randomChoice);


    }

    Monster(String newName, int newHealth, int monsterType) {
        super(newName,newHealth);
        // make button 34 wide and 55 high, size of each sprite image
        setBounds(0, 0, 64, 64);
        // make custom monster
        generateMonster(monsterType);
        // set name
        setSpriteName(newName);

    }

    void generateMonster(int type) {
        if (deBug) System.out.println("Making Monster " + type);

        if (type == 0) {
            // get new hero sprite page from images
            try {
                monsterImages = ImageIO.read((this.getClass().getResource("images/monster1.png")));
            } catch (IOException e) {
                System.out.println("monster1.png not found?!");
            }
            // create a new sprite with name Hero and health of 100
            // this monster uses a handheld weapon
            setWeaponType(1);
            setImages(monsterImages, 1);
            // set name to 1
            setSpriteName(monsterName[0]);

        } else if (type == 1) {
            // get new hero sprite page from images
            try {
                monsterImages = ImageIO.read(this.getClass().getResource("images/monster2.png"));
            } catch (IOException e) {
                System.out.println("monster2.png not found?!");
            }
            // this monster uses a bow
            setWeaponType(2);
            // create a new sprite with name Hero and health of 100
            setImages(monsterImages, 1);
            // set name to 1
            setSpriteName(monsterName[1]);
        } else if (type == 2) {
            // get new hero sprite page from images
            try {
                monsterImages = ImageIO.read(this.getClass().getResource("images/monster3.png"));
            } catch (IOException e) {
                System.out.println("monster3.png not found?!");
            }
            // this monster uses a bow
            setWeaponType(2);
            // create a new sprite with name Hero and health of 100
            setImages(monsterImages, 1);
            // set name to 1
            setSpriteName(monsterName[2]);
        } else if (type == 3) {
            // get new hero sprite page from images
            try {
                monsterImages = ImageIO.read(this.getClass().getResource("images/drevil.png"));
            } catch (IOException e) {
                System.out.println("drevil.png not found?!");
            }
            // dr evil uses handheld weapon
            setWeaponType(3);
            // create a new sprite with name Hero and health of 100
            setImages(monsterImages, 1);
        }
    }

}
