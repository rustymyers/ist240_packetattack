/**
* @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: CreditPane.java
 * Short Description: Credit Pane for Final Project
**/
import java.awt.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class CreditPane extends JPanel
{
	// class parameters
	public JButton backButton;
	JLabel credits, rusty, joshua, creditText, rustyPicArea, joshuaPicArea;
    JEditorPane joshuaBio, rustyBio, rustyRoles, joshuaRoles;
	
	// JButton startButton;
	public CreditPane()
	{
		// create new panel from superclass
		super();
		// create local variable for use
		Dimension size;
		// set layout to border for jlabel
		setLayout(new BorderLayout());
		// Create image icon from local file
		ImageIcon backgroundImage = new ImageIcon(this.getClass().getResource("images/Flat_earth_night.jpg"));
		// Add  castle image to JLabel
		JLabel backgroundLabel = new JLabel(backgroundImage);
		// Add new castle image jlabel to menu panel
		add(backgroundLabel);
		// set castle image jlabel layout to Null
		backgroundLabel.setLayout(null);
		// create back button
		backButton = new JButton("Back");
		// add back button to castle jlabel
		backgroundLabel.add(backButton);
		//
		// END DEFAULT WORK
		//
		
		//
		// START CUSTOM PANEL WORK
		// create credits jlabel
		credits = new JLabel("<html><b>The Creators of Packet Attack</b><br><br>");
		// Sets jlabel font color
		credits.setOpaque(false);
		credits.setForeground(Color.WHITE);
		// Add credits jlabel to castle jlabel
		credits.setFont(new Font("Serif", Font.ITALIC, 30));
        // add new label to background
        backgroundLabel.add(credits);
		// get preferred size of credits label
		size = credits.getPreferredSize();
		// set size and placement of credits label- (starting x, starting y, x width, y width)
		credits.setBounds(640-(size.width/2),30,size.width, size.height);

        // Rusty Section //
        rusty = creditHeaders("<html><b>Rusty Myers - Bug Squasher</b><br><br>");
        // rusty.setFont(new Font("Serif", Font.ITALIC, 20));
        backgroundLabel.add(rusty);
        // get the preferred size of the button
        size = rusty.getPreferredSize();
        // set size and placement of credits label- (starting x, starting y, x width, y width)
        rusty.setBounds(300-(size.width/2),150,size.width, size.height);
        // Create a new image
        ImageIcon rustyPic = new ImageIcon(this.getClass().getResource("images/rustyPic.jpg"));
        // Create new JLabel
        rustyPicArea = new JLabel();
        // add new JLabel to background
        backgroundLabel.add(rustyPicArea);
        // Set the background to the picture
        rustyPicArea.setIcon(rustyPic);
        // get preferred size
        size = rustyPicArea.getPreferredSize();
        // set size and placement of credits label- (starting x, starting y, x width, y width)
        rustyPicArea.setBounds(170-(size.width/2),200,size.width, size.height);
        // Create editor pane for bios
        rustyBio = new JEditorPane("text/html", null);
        // set pane to be editable
        rustyBio.setEditable(false);
        // set opague setting
        rustyBio.setOpaque(false);
        // set preferred size
        rustyBio.setPreferredSize(new Dimension(300, 300));
        // add new bio to background label
        backgroundLabel.add(rustyBio);
        // set the text to the bio
        rustyBio.setText("<html><b><font color=#FFFFFF>Rusty is an IT Professional specializing in systems deployment. He also enjoys presenting on IT related tools and\n" +
                "services, as well as working with other professionals to improve processes with ITIL. His spare time is spent hiking\n" +
                "and camping with his wife and four children.<br>Many Thanks to Judah and Brodie for bug testing and constant inspiration.</font></b></html>");
        // get the preferred size
        size = rustyBio.getPreferredSize();
        // set size and placement of credits label- (starting x, starting y, x width, y width)
        rustyBio.setBounds(420-(size.width/2),200,size.width, size.height);
        // create editor pane for bio
        rustyRoles = new JEditorPane("text/html", null);
        // set pane to be editable
        rustyRoles.setEditable(false);
        // set opaque to false
        rustyRoles.setOpaque(false);
        // set the preferred size
        rustyRoles.setPreferredSize(new Dimension(300, 300));
        // add label to background
        backgroundLabel.add(rustyRoles);
        // set the text of the role pane
        rustyRoles.setText("<html><b><font color=#FFFFFF>" +
                            "<u>Developmental Roles</u><br/>" +
                            "<ul>Lead Developer</ul>" +
                            "<ul>Problem Solver</ul>" +
                            "<ul>Creative Director</ul>" +
                            "</font></b></html>");
        // get the preferred size for roles
        size = rustyRoles.getPreferredSize();
        // set size and placement of credits label- (starting x, starting y, x width, y width)
        rustyRoles.setBounds(250-(size.width/2),380,size.width, size.height);
        // Joshua Section //
        joshua = creditHeaders("<html><b>Joshua Mathews - Virus Deflector</b><br><br>");
        // add new label to the background
        backgroundLabel.add(joshua);
        // get the preferred size of the new label
        size = joshua.getPreferredSize();
        // set size and placement of credits label- (starting x, starting y, x width, y width)
        joshua.setBounds(930-(size.width/2),150,size.width, size.height);
        // create new image icon
        ImageIcon joshuaPic = new ImageIcon(this.getClass().getResource("images/joshuaPic.jpg"));
        joshuaPicArea = new JLabel();
        backgroundLabel.add(joshuaPicArea);
        joshuaPicArea.setIcon(joshuaPic);
        size = joshuaPicArea.getPreferredSize();
        // set size and placement of credits label- (starting x, starting y, x width, y width)
        joshuaPicArea.setBounds(800-(size.width/2),200,size.width, size.height);
        // create new pane for bio
        joshuaBio = new JEditorPane("text/html", null);
        // set pane editable to false
        joshuaBio.setEditable(false);
        // set pane opaqe to false
        joshuaBio.setOpaque(false);
        // set preferred size
        joshuaBio.setPreferredSize(new Dimension(300, 300));
        // add new bio to background
        backgroundLabel.add(joshuaBio);
        // set text of new pane
        joshuaBio.setText("<html><b><font color=#FFFFFF>Joshua works in the technology division of a document solutions company He enjoys playing golf when he can find time. " +
                "He is looking forward to graduation for a career change. He would like to become a developer at Boeing. In his free time he enjoys traveling with his wife " +
                "and two sons, and playing video games (again, when he can find time.</font></b></html>");
        // set pane preferred size
        size = joshuaBio.getPreferredSize();
        // set size and placement of credits label- (starting x, starting y, x width, y width)
        joshuaBio.setBounds(1050-(size.width/2),200,size.width, size.height);
        // create new role pane
        joshuaRoles = new JEditorPane("text/html", null);
        // set editable to false
        joshuaRoles.setEditable(false);
        // set opaque to false
        joshuaRoles.setOpaque(false);
        // set preferred size
        joshuaRoles.setPreferredSize(new Dimension(300, 300));
        // add pane to background
        backgroundLabel.add(joshuaRoles);
        // set text of pane
        joshuaRoles.setText("<html><b><font color=#FFFFFF>" +
                            "<u>Developmental Roles</u><br/>" +
                            "<ul>Developer</ul>" +
                            "<ul>Problem Creator</ul>" +
                            "<ul>Imaginative Enthusiast</ul>" +
                            "</font></b></html>");
        // set preferred size of pane
        size = joshuaRoles.getPreferredSize();
        // set size and placement of credits label- (starting x, starting y, x width, y width)
        joshuaRoles.setBounds(880-(size.width/2),380,size.width, size.height);
        // get prefered size of back button
		size = backButton.getPreferredSize();
		// set size and placement of back button- (starting x, starting y, x width, y width)
		backButton.setBounds(640-(size.width/2),600,size.width, size.height);
	}
    // Methods Start //
    JLabel creditHeaders(String labelText)
    {
        // create new label for method
        creditText = new JLabel(labelText);
        // Set Opaque default setting
        creditText.setOpaque(false);
        // Set Border default setting
        creditText.setBorder(new EmptyBorder(5, 5, 5, 5));
        // Set foreground default setting
        creditText.setForeground(Color.WHITE);
        // Set Font default setting
        creditText.setFont(new Font("Serif", Font.ITALIC, 20));
        // Return new label
        return creditText;
    }
}