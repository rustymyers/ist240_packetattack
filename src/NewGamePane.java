/**
* @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: NewGamePane.java
 * Short Description: New Game Panel for Final Project
**/

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.Rectangle;

public class NewGamePane extends JPanel implements ActionListener {

    // Debugging flags
    Boolean deBug = false;
    Boolean deBugBounds = false;
    Boolean deBugTreasure = false;

    // class parameters
    public JButton quitGameButton, nextLevel, prevLevel;
    JLabel introduction, gameBackground;
    public SettingsPane settings;
    Timer gameTimer, monsterAnimationTimer, heroAnimationTimer, heroTimer;
    int delay, loop;
    // introDelay sets the number of seconds to delay the story window
    int introDelay = 15;
    int moveLoop = 0;
    Boolean startGame = false;
    Boolean genLevelDone = false;
    Boolean endGameBool = false;
    MainFrame mainFrame;
    Boolean skipLoop = false;
    JLabel levelLabel, textConsole;

    Hero hero, maleHero, femaleHero;
    String chosenHero = "none";
    int heroSexChoice;
    Boolean rightInBounds = false;
    Boolean leftInBounds = false;
    Boolean upInBounds = false;
    Boolean downInBounds = false;
    Boolean rightCorridorInBounds = false;
    Boolean leftCorridorInBounds = false;
    Boolean upCorridorInBounds = false;
    Boolean downCorridorInBounds = false;
    Boolean dontMoveRight, dontMoveLeft, dontMoveUp, dontMoveDown;
    Boolean monsterNear = false;

    Rectangle[][] allTheLevels;
    Rectangle[][] allCorridorBounds;
    Rectangle[][] allCorridorBounds2;
    Rectangle upStairsRect, downStairsRect;
    int numberOfLevels, roomSize, currentLevel;
    BufferedImage roomTile, upStairsTile, downStairsTile;

    int roomTileW, roomTileH, tempW, tempH, upStairsTileH, upStairsTileW, downStairsTileH, downStairsTileW;
    JProgressBar heroHealth;
    JProgressBar[][] monsterHealth;
    Rectangle[][] monsterHealthLocation;

    int heroHealthW, heroHealthH, monsterHealthW, monsterHealthH, monsterNumbers;
    Monster currentMonster;
    int currentMonsterInt;
    Monster[][] levelMonsters;
    Rectangle[][] levelMonstersLocation;
    Boolean currentLevelMonsterAdded[];
    Treasure levelTreasure[][];
    Monster DrEvilPacket;
    Boolean DrEvilAdded = false;
    Boolean DrEvilNear = false;

    JLabel textOutput;
    JTextField newHeroNameField;
    JButton heroSetButton;
    JLabel maleStart, femaleStart;
    JLabel nameInstruction;

    int monsterPoints = 5;
    int treasurePoints = 10;
    int drevilPoints = 20;

    XML_240 xmlSave;
    String xmlFile = "PacketAttackHighScores.xml";

    Sound treasurePickUp, bowFire, heartBeat, swordMiss, swordHit, doorOpens, monsterDamage, monsterMiss, evilMiss, evilAttack, defeatedSound, victory, drEvilPacket;
    String messageBuffer = "";

    JLabel viewHighScores;
    Boolean panelAdded = false;
    public JButton viewScoresButton = new JButton("View Scores");

    public NewGamePane(SettingsPane newSettings, JButton newQuitGameButton, MainFrame newMainFrame) {
        // create new panel from superclass
        super();
        if (deBug) System.out.println("Starting a new game");
        // create new JLabel for viewing High Scores
        viewHighScores = new JLabel();

        mainFrame = newMainFrame;

        //------TIMER -------------------------------------------
        delay = 1000; //milliseconds
        // timer for the main game
        gameTimer = new Timer(delay, this);
        delay = 200;
        // timer for monster fighting
        monsterAnimationTimer = new Timer(delay, this);
        delay = 100;
        // timer for hero fighting
        heroAnimationTimer = new Timer(delay, this);
        // timer for hero selection screen
        heroTimer = new Timer(delay, this);

        // set global settings variable
        settings = newSettings;
        quitGameButton = newQuitGameButton;
        viewScoresButton.addActionListener(this);

        // set layout to border for jlabel
        setLayout(new BorderLayout());

        // Create image icon from local file
        ImageIcon backgroundImage = new ImageIcon(this.getClass().getResource("images/drevilpacket.jpg"));
        // Add scaled castle image to JLabel
        gameBackground = new JLabel(backgroundImage);


        // set castle image jlabel layout to Null
        gameBackground.setLayout(null);

        femaleStart = new JLabel("Female");
        femaleStart.setBounds((int)(mainFrame.getWidth()*.75)-25,400,50,20);
        femaleStart.setForeground(Color.WHITE);
        femaleStart.setBackground(Color.BLACK);
        femaleStart.setOpaque(true);
        femaleStart.setAlignmentY(50);

        femaleHero = new Hero(" Female",0);
        femaleHero.setBounds((int)(mainFrame.getWidth()*.75)-50,300,100,100);
        femaleHero.addActionListener(mainFrame);

        maleStart = new JLabel("   Male");
        maleStart.setBounds((int)(mainFrame.getWidth()*.25)-25,400,50,20);
        maleStart.setForeground(Color.WHITE);
        maleStart.setBackground(Color.BLACK);
        maleStart.setOpaque(true);
        maleStart.setAlignmentY(50);

        maleHero = new Hero("male",1);
        maleHero.setBounds((int)(mainFrame.getWidth()*.25)-50,300,100,100);
        maleHero.addActionListener(mainFrame);

        heroSetButton = new JButton("Continue...");
        heroSetButton.addActionListener(mainFrame);
        heroSetButton.setBounds((int) (mainFrame.getWidth() * .5) - 50, 600, 100, 20);

        textOutput = new JLabel();
        textOutput.setBounds((int) (mainFrame.getWidth() * .5) - 100, 340, 200, 30);
        textOutput.setAlignmentY(textOutput.getWidth() / 2);
        textOutput.setForeground(Color.WHITE);

        nameInstruction = new JLabel("Select a Character and Enter your name!");
        nameInstruction.setBounds((int) (mainFrame.getWidth() * .5) - 125, 270, (int)nameInstruction.getPreferredSize().getWidth(), 30);
        nameInstruction.setForeground(Color.WHITE);

        newHeroNameField = new JTextField();
        newHeroNameField.setBounds((int) (mainFrame.getWidth() * .5) - 125, 300, 250, 30);
        newHeroNameField.addKeyListener(mainFrame);

        gameBackground.add(maleHero);
        gameBackground.add(femaleHero);
        gameBackground.add(maleStart);
        gameBackground.add(femaleStart);
        gameBackground.add(heroSetButton);
        gameBackground.add(textOutput);
        gameBackground.add(newHeroNameField);
        gameBackground.add(nameInstruction);

        // Add new castle image jlabel to menu panel
        add(gameBackground);
        if (deBug) System.out.println("Added background");

        repaint();

        //This sound is used when the Hero picks up a treasure.
        treasurePickUp = new Sound((this.getClass().getResource("sounds/coin.wav")));
        treasurePickUp.init();
        treasurePickUp.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used when the Hero shoots the bow
        bowFire = new Sound((this.getClass().getResource("sounds/bowfire.wav")));
        bowFire.init();
        bowFire.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used when the Hero is at low health
        heartBeat = new Sound((this.getClass().getResource("sounds/heartbeat-trimmed.wav")));
        heartBeat.init();
        heartBeat.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used when the Hero uses the sword
        swordHit = new Sound((this.getClass().getResource("sounds/swordhit.wav")));
        swordHit.init();
        swordHit.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used when the Hero misses an attack
        swordMiss = new Sound((this.getClass().getResource("sounds/swordmiss.wav")));
        swordMiss.init();
        swordMiss.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used when the Hero moves to the next level
        doorOpens = new Sound((this.getClass().getResource("sounds/door_opens.wav")));
        doorOpens.init();
        doorOpens.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used when the monster does damage
        monsterDamage = new Sound((this.getClass().getResource("sounds/monsterDamage.wav")));
        monsterDamage.init();
        monsterDamage.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used when the monster misses an attach
        monsterMiss = new Sound((this.getClass().getResource("sounds/swordmiss.wav")));
        monsterMiss.init();
        monsterMiss.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used when Dr. Evil attacks
        evilAttack = new Sound((this.getClass().getResource("sounds/evilAttack.wav")));
        evilAttack.init();
        evilAttack.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used when Dr. Evil misses an attack
        evilMiss = new Sound((this.getClass().getResource("sounds/evilMiss.wav")));
        evilMiss.init();
        evilMiss.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used when hero has been defeated
        defeatedSound = new Sound((this.getClass().getResource("sounds/PowerDown.wav")));
        defeatedSound.init();
        defeatedSound.setVolume(mainFrame.settings.getSoundVolumeRequested());

        //This sound is used for hero victory
        victory = new Sound((this.getClass().getResource("sounds/victory.wav")));
        victory.init();
        victory.setVolume(mainFrame.settings.getSoundVolumeRequested());

        // Sound used when close to Dr Evil Packet
        drEvilPacket = new Sound((this.getClass().getResource("sounds/DrEvilPacket.wav")));
        drEvilPacket.init();
        drEvilPacket.setVolume(mainFrame.settings.getSoundVolumeRequested());
    }

    // actionPerformed
    public void actionPerformed(ActionEvent event) {
        Object obj = event.getSource();

        // create a new game after setting hero
        if (obj == heroSetButton) {
            startGameStory();
        }

        // every tick of the timer clock will call the actionperformed method
        if (obj == gameTimer) {

            // add one every time we trigger the timer. helps keep track of how long we've been on the screen
            loop += 1;

            // System.out.println("Loop "+ loop);
            // remove the background and labels after a few times
            if (getSkipLoop() && loop <= introDelay) {
                // move to game
                if (deBug) System.out.println("Loop skipped with space bar");
                loop = (introDelay + 1);
                setSkipLoop(false);
                startGame = true;
            }
            if (loop == introDelay) {
                if (deBug) System.out.println("Loop reached 5");
                setSkipLoop(false);
                startGame = true;
            }

            if (startGame) {
                if (deBug) System.out.println("Make a new level");
                // Get number of levels from settings
                numberOfLevels = settings.getNumberOfLevels();
                if (deBug) System.out.println("Number of levels: " + numberOfLevels);
                // create the correct size of room for all the requested levels
                if (deBug) System.out.println("Requested Map Size: " + settings.getMapSize());
                // Start with making a small map
                if (settings.getMapSize().equals("Small")) {
                    // Number of small rooms
                    roomSize = 5;
                }
                if (settings.getMapSize().equals("Medium")) {
                    // Number of small rooms
                    roomSize = 10;
                }
                if (settings.getMapSize().equals("Large")) {
                    // Number of small rooms
                    roomSize = 15;
                }
                if (deBug) System.out.println("Number of Rooms: " + roomSize);


                // Get the difficulty and set the number of monsters based on that
                String diffSetting = settings.getDiffSetting();
                if (deBug) System.out.println("Difficulty Setting: " + diffSetting);
                switch (diffSetting) {
                    case "Easy":
                        monsterNumbers = 5;
                        break;
                    case "Medium":
                        monsterNumbers = 10;
                        break;
                    case "Hard":
                        monsterNumbers = 15;
                        break;
                }
                if (deBug) System.out.println("Number of Monsters: " + monsterNumbers);

                // remove old content
                settings.mainFrame.getContentPane().removeAll();

                remove(gameBackground);
                remove(introduction);


                // set panel back to null layout
                setLayout(null);
                setBackground(new Color(24, 47, 101));

                // add hero
                add(hero);
                // add evil packet
                add(DrEvilPacket);

                if (settings.getHeroHealthBarVisable()) {
                    heroHealth = new JProgressBar(0, hero.getHealth());
                    UIManager.put("ProgressBar.background", Color.RED);
                    UIManager.put("ProgressBar.foreground", Color.RED);
                    UIManager.put("ProgressBar.selectionBackground", Color.RED);
                    UIManager.put("ProgressBar.selectionForeground", Color.RED);
                    heroHealthW = 50;
                    heroHealthH = 5;
                    add(heroHealth);
                    heroHealth.setValue(hero.getHealth());
                    heroHealth.setStringPainted(true);
                    heroHealth.setString("");
                    heroHealth.setForeground(Color.RED);
                }

                quitGameButton.setBounds(1200, 670, 80, 30);
                add(quitGameButton);
                viewScoresButton.setBounds(1085, 670, 110, 30);
                add(viewScoresButton);
                levelLabel.setBounds(1095, 10, 170, 30);
                add(levelLabel);
                textConsole.setBounds(1095, 40, 190, 200);
                add(textConsole);
                textConsole.setVerticalAlignment(JLabel.TOP);


                currentLevel = 0;
                // get current rooms from genLevel
                genLevel();
                // set hero to be inside room 0
                hero.setBounds((int) allTheLevels[currentLevel][0].getX() + 20, (int) allTheLevels[currentLevel][0].getY() + 20, hero.getWidth(), hero.getHeight());

                // paint all our objects
                repaint();

                startGame = false;

            }


        }
        if (obj == monsterAnimationTimer) {
            updateMonsters();
        }
        if (obj == heroAnimationTimer) {
            Boolean doDamage;


            if (monsterNear) {
                Monster currentMonsterAttack = levelMonsters[currentLevel][currentMonsterInt];
                JProgressBar monsterHealthBar = monsterHealth[currentLevel][currentMonsterInt];
                if (currentMonsterAttack.getBounds().getCenterX() > hero.getBounds().getCenterX()) {
                    // monster is to the right
                    if (deBug) System.out.println("Monster is right");
                    doDamage = hero.attackRight();
                } else {
                    if (deBug) System.out.println("Monster is left");
                    doDamage = hero.attackLeft();
                }
                if (doDamage) {
                    // do damage based on hero's attack value
                    int damage = makeRandomInt(hero.getAttackDamage());
                    if (damage > 0) {
                        currentMonsterAttack.reduceHealth(damage);
                        if (settings.getMonsterHealthBarVisable()) {
                            monsterHealthBar.setValue(currentMonsterAttack.reduceHealth(damage));
                        }

                        if (hero.getWeaponType() == 1) {
                            swordHit.playMusic();
                        } else if (hero.getWeaponType() == 2) {
                            bowFire.playMusic();
                        } else {
                            swordHit.playMusic();
                        }
                        updateTextConsole("<font color=\"#99ff99\">" + hero.getSpriteName() + " damaged " + currentMonsterAttack.getSpriteName() + " with " + damage + " damage</font>");
                        if (deBug) System.out.println(hero.getSpriteName() + " damaged " + currentMonsterAttack.getSpriteName() + " with " + damage + " damage");
                    } else {
                        swordMiss.playMusic();
                        updateTextConsole("<font color=\"#99ff99\">" + hero.getSpriteName() + " missed " + currentMonsterAttack.getSpriteName() + "!</font>");
                        if (deBug) System.out.println(hero.getSpriteName() + " missed " + currentMonsterAttack.getSpriteName() + "!");
                    }
                    hero.setIcon(hero.getCurrentSpriteImage());
                    heroAnimationTimer.stop();
                }
            } else if (DrEvilNear) {
                if (DrEvilPacket.getBounds().getCenterX() > hero.getBounds().getCenterX()) {
                    // monster is to the right
                    if (deBug) System.out.println("DrEvilPacket is right");
                    doDamage = hero.attackRight();
                } else {
                    if (deBug) System.out.println("DrEvilPacket is left");
                    doDamage = hero.attackLeft();
                }
                if (doDamage) {
                    // do damage based on hero's attack value
                    int damage = makeRandomInt(hero.getAttackDamage());
                    if (damage > 0) {
                        if (hero.getWeaponType() == 1) {
                            swordHit.playMusic();
                        } else if (hero.getWeaponType() == 2) {
                            bowFire.playMusic();
                        } else {
                            swordHit.playMusic();
                        }
                        DrEvilPacket.reduceHealth(damage);
                        updateTextConsole("<font color=\"#99ff99\">" + hero.getSpriteName() + " damaged " + DrEvilPacket.getSpriteName() + " with " + damage + " damage<font>");
                        if (deBug)
                            System.out.println(hero.getSpriteName() + " damaged " + DrEvilPacket.getSpriteName() + " with " + damage + " damage");
                    } else {
                        updateTextConsole("<font color=\"#99ff99\">" + hero.getSpriteName() + " missed " + DrEvilPacket.getSpriteName() + "!</font>");
                        if (deBug)
                            System.out.println(hero.getSpriteName() + " missed " + DrEvilPacket.getSpriteName() + "!");
                    }
                    hero.setIcon(hero.getCurrentSpriteImage());
                    heroAnimationTimer.stop();
                }
            } else {
                if (deBug) System.out.println("Hero not hear any monsters");
            }
        }
        if (obj == heroTimer) {
            Hero movingHero;
            if (chosenHero.contentEquals("male")) {
                movingHero = maleHero;
            } else {
                movingHero = femaleHero;
            }
            if (moveLoop > 8) {
                moveLoop = 0;
            }
            ImageIcon tempImage = movingHero.walkDown[moveLoop];
            movingHero.setCurrentSpriteImage(tempImage);
            movingHero.setSpriteImage(tempImage);
            moveLoop += 1;
        }



        if (obj == nextLevel) {
            setNextLevel();
        }
        if (obj == prevLevel) {
            setPrevLevel();
        }

        if (obj == viewScoresButton) {
            if (panelAdded) {
                showHighScores(false);
            } else {
                showHighScores(true);
            }
        }
    }


    public void genLevel() {
        Rectangle[] currentRooms;
        Rectangle[] corridorBounds;
        Rectangle[] corridorBounds2;

        // create double array for all the levels and all the monsters
        levelMonsters = new Monster[numberOfLevels][monsterNumbers];
        // create double array for all the levels and all the monster health bars
        monsterHealth = new JProgressBar[numberOfLevels][monsterNumbers];
        levelMonstersLocation = new Rectangle[numberOfLevels][monsterNumbers];
        monsterHealthLocation = new Rectangle[numberOfLevels][monsterNumbers];
        levelTreasure = new Treasure[numberOfLevels][monsterNumbers];
        currentLevelMonsterAdded = new Boolean[numberOfLevels];
        Arrays.fill(currentLevelMonsterAdded, false);

        if (deBug) System.out.println("Generating " + numberOfLevels + "levels.");
        // B&W Circuit board
        roomTile = cacheImage(this.getClass().getResource("images/board2.png"));
        roomTileH = tempH;
        roomTileW = tempW;
        upStairsTile = cacheImage(this.getClass().getResource("images/gamestairs3.png"));
        upStairsTileH = tempH;
        upStairsTileW = tempW;
        downStairsTile = cacheImage(this.getClass().getResource("images/gamestairs4.png"));
        downStairsTileH = tempH;
        downStairsTileW = tempW;

        // If we're not already Done, then we make a new map
        if (!genLevelDone) {
            // create the array for all the levels rooms
            allTheLevels = new Rectangle[numberOfLevels][100];
            allCorridorBounds = new Rectangle[numberOfLevels][200];
            allCorridorBounds2 = new Rectangle[numberOfLevels][200];
            for (int level = 0; level < numberOfLevels; level++) {
                if (deBug) System.out.println("level " + level);
                // Create new object, send main frame bounds to new object
                GenerateRooms newDimensions = new GenerateRooms(settings.mainFrame.getBounds());
                // Generate the new room dimensions for level
                currentRooms = newDimensions.genRooms(roomSize);
                // get corridors in temp array
                corridorBounds = newDimensions.getCorridorBounds();
                corridorBounds2 = newDimensions.getCorridorBounds2();

                // place level rooms into double array
                for (int j = 0; j < currentRooms.length; j++) {
                    if (deBug) System.out.println("current level " + level + " current rooms " + j);
                    if (currentRooms[j] != null) {
                        allTheLevels[level][j] = currentRooms[j];
                    }
                }
                // place level corridors into double array
                for (int j = 0; j < corridorBounds.length; j++) {
                    if (corridorBounds[j] != null) {
                        allCorridorBounds[level][j] = corridorBounds[j];
                    }
                }
                // place level corridors into double array
                for (int j = 0; j < corridorBounds2.length; j++) {

                    if (corridorBounds2[j] != null) {
                        allCorridorBounds2[level][j] = corridorBounds2[j];
                    }
                }

                // create monsters for each room
                for (int monster = 0; monster < monsterNumbers; monster++) {
                    // create monsters
                    if (deBug) System.out.println("Creating monster " + monster + " of " + monsterNumbers);
                    // create monsters for each room, make health based on current level
                    levelMonsters[level][monster] = new Monster("Level" + level, (makeRandomInt((level * 3)) + 5), false);
                    if (settings.getMonsterHealthBarVisable()) {
                        monsterHealth[level][monster] = new JProgressBar(0, levelMonsters[level][monster].getHealth());
                        monsterHealthW = 50;
                        monsterHealthH = 5;
                        add(monsterHealth[level][monster]);
                        monsterHealth[level][monster].setValue(levelMonsters[level][monster].getHealth());
                        monsterHealth[level][monster].setStringPainted(true);
                        monsterHealth[level][monster].setString("");
                    }

                    // Also create treasure for each (some may be empty)
                    if (deBug) System.out.println("Creating treasure " + monster + " of " + monsterNumbers);
                    levelTreasure[level][monster] = new Treasure();
                }
                // create monster rectangles for each level
                int monsterPlaced = 0;
                Boolean[] spot1Empty = new Boolean[currentRooms.length];
                Arrays.fill(spot1Empty, true);
                Boolean[] spot2Empty = new Boolean[currentRooms.length];
                Arrays.fill(spot2Empty, true);
                Boolean[] spot3Empty = new Boolean[currentRooms.length];
                Arrays.fill(spot3Empty, true);
                Boolean[] spot4Empty = new Boolean[currentRooms.length];
                Arrays.fill(spot4Empty, true);

                // For each level, while we still have monsters to place
                while (monsterPlaced < monsterNumbers) {
                    // add monster
                    for (int room = 0; room < roomSize; room++) {
                        // if level 0 and room 0, spare the player
                        if (level == 0 && room == 0) {
                            if (deBug) System.out.println("Monster " + monsterPlaced + " doesn't go in level 0 room 0");
                        } else {
                            if (deBug)
                                System.out.println("Placing monster : " + monsterPlaced + " of " + (monsterNumbers - 1));

                            // add(levelMonsters[currentLevel][monsterPlaced]);

                            if (deBug)
                                System.out.println("Ready to place monster " + monsterPlaced + " in room " + room + " of " + roomSize);
                            if (spot1Empty[room]) {
                                if (deBug) System.out.println("Place monster in top left");
                                spot1Empty[room] = false;
                                levelMonstersLocation[level][monsterPlaced] = new Rectangle(
                                        (int) allTheLevels[level][room].getX(),
                                        (int) allTheLevels[level][room].getY(),
                                        levelMonsters[level][monsterPlaced].getWidth(),
                                        levelMonsters[level][monsterPlaced].getHeight());
                                // add health bar
                                if (settings.getMonsterHealthBarVisable()) {
                                    if (deBug) System.out.println("Placing health bar for monster " + monsterPlaced);
                                    // add(monsterHealth[currentLevel][monsterPlaced]);
                                    monsterHealthLocation[level][monsterPlaced] = new Rectangle(
                                            (int) levelMonstersLocation[level][monsterPlaced].getBounds().getX() + 5,
                                            (int) levelMonstersLocation[level][monsterPlaced].getBounds().getY() + 5,
                                            50, 5);
                                }
                                monsterPlaced += 1;
                                if (monsterPlaced >= monsterNumbers) break;

                            } else if (spot2Empty[room]) {
                                if (deBug) System.out.println("Place monster in bottom left");
                                spot2Empty[room] = false;
                                levelMonstersLocation[level][monsterPlaced] = new Rectangle(
                                        (int) allTheLevels[level][room].getX() + 10,
                                        (int) allTheLevels[level][room].getMaxY() - (10 + levelMonsters[level][monsterPlaced].getHeight()),
                                        levelMonsters[level][monsterPlaced].getWidth(),
                                        levelMonsters[level][monsterPlaced].getHeight());
                                // add health bar
                                if (settings.getMonsterHealthBarVisable()) {
                                    if (deBug) System.out.println("Placing health bar for monster " + monsterPlaced);
                                    // add(monsterHealth[currentLevel][monsterPlaced]);
                                    monsterHealthLocation[level][monsterPlaced] = new Rectangle(
                                            (int) levelMonstersLocation[level][monsterPlaced].getX(),
                                            (int) levelMonstersLocation[level][monsterPlaced].getY() + 10,
                                            50, 5);
                                }
                                monsterPlaced += 1;
                                if (monsterPlaced >= monsterNumbers) break;

                            } else if (spot3Empty[room]) {
                                if (deBug) System.out.println("Place monster in top right");
                                spot3Empty[room] = false;
                                levelMonstersLocation[level][monsterPlaced] = new Rectangle(
                                        (int) allTheLevels[level][room].getMaxX() - (10 + levelMonsters[level][monsterPlaced].getWidth()),
                                        (int) allTheLevels[level][room].getY(),
                                        levelMonsters[level][monsterPlaced].getWidth(),
                                        levelMonsters[level][monsterPlaced].getHeight());
                                // add health bar
                                if (settings.getMonsterHealthBarVisable()) {
                                    if (deBug) System.out.println("Placing health bar for monster " + monsterPlaced);
                                    monsterHealthLocation[level][monsterPlaced] = new Rectangle(
                                            (int) levelMonstersLocation[level][monsterPlaced].getX(),
                                            (int) levelMonstersLocation[level][monsterPlaced].getY() + 10,
                                            50, 5);
                                }
                                monsterPlaced += 1;
                                if (monsterPlaced >= monsterNumbers) break;

                            } else if (spot4Empty[room]) {
                                if (deBug) System.out.println("Place monster in bottom right");
                                spot4Empty[room] = false;
                                levelMonstersLocation[level][monsterPlaced] = new Rectangle(
                                        (int) allTheLevels[level][room].getMaxX() - (levelMonsters[level][monsterPlaced].getWidth() + 10),
                                        (int) allTheLevels[level][room].getMaxY() - (10 + levelMonsters[level][monsterPlaced].getHeight()),
                                        levelMonsters[level][monsterPlaced].getWidth(),
                                        levelMonsters[level][monsterPlaced].getHeight());
                                // add health bar
                                if (settings.getMonsterHealthBarVisable()) {
                                    if (deBug) System.out.println("Placing health bar for monster " + monsterPlaced);
                                    monsterHealthLocation[level][monsterPlaced] = new Rectangle(
                                            (int) levelMonstersLocation[level][monsterPlaced].getX(),
                                            (int) levelMonstersLocation[level][monsterPlaced].getY() + 10,
                                            50, 5);
                                }
                                monsterPlaced += 1;
                                if (monsterPlaced >= monsterNumbers) break;

                            } else {
                                if (deBug)
                                    System.out.println("We can't place this monster in any room: " + monsterPlaced);
                            }


                        }

                    }
                    if (deBug) System.out.println("While Loop at End. Level: " + level + " Number: " + monsterPlaced);
                }
            }
        }
    }

    // Create and return a random int. Pass a range to it
    int makeRandomInt(double range) {
        // create random value
        double r = Math.random();
        // create new value
        int myrandomNumber;
        // Create range between 0 and 5
        myrandomNumber = (int) (r * range);
        // Return value
        return myrandomNumber;
    }

    // Grab our images
    BufferedImage cacheImage(URL filePath) {
        BufferedImage newImage;
        try {
            newImage = ImageIO.read(filePath);
            tempW = newImage.getWidth(null);
            tempH = newImage.getHeight(null);
            BufferedImage bi2 = new BufferedImage(tempW, tempH, BufferedImage.TYPE_INT_RGB);
            Graphics big = bi2.getGraphics();
            big.drawImage(newImage, 0, 0, null);
            newImage = bi2;
            if (deBug) System.out.println("new image is " + tempW + "x" + tempH);

        } catch (IOException e) {
            newImage = null;
        }

        return newImage;
    }

    // paint stuff
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        rightInBounds = false;
        leftInBounds = false;
        upInBounds = false;
        downInBounds = false;
        rightCorridorInBounds = false;
        leftCorridorInBounds = false;
        upCorridorInBounds = false;
        downCorridorInBounds = false;
        UIManager.put("ProgressBar.background", Color.RED);
        UIManager.put("ProgressBar.foreground", Color.RED);
        UIManager.put("ProgressBar.selectionBackground", Color.RED);
        UIManager.put("ProgressBar.selectionForeground", Color.RED);
        // print the current level if it's less than all the levels
        if (currentLevel < numberOfLevels) {
            if (deBug) System.out.println("Current Level " + currentLevel);
            // set the level label to the correct number + 1 of array offset
            levelLabel.setText("Level " + (currentLevel + 1));
            // if we've created corridors
            if (allCorridorBounds != null) {
                // for each of the corridors in this level
                for (int i = 0; i < allCorridorBounds[currentLevel].length; i++) {
                    if (deBug) System.out.println("Writing Corridor: " + i);
                    // store the arrays corridors in a rectangle
                    Rectangle tempBounds = allCorridorBounds[currentLevel][i];
                    // if this corridor is not null
                    if (tempBounds != null) {
                        int X = (int) tempBounds.getX();
                        int Y = (int) tempBounds.getY();
                        // create a colored rectangle
                        g.setColor(new Color(50, 205, 50));
                        g.fillRect(X, Y, (int) tempBounds.getWidth(), (int) tempBounds.getHeight());

                        // testing location tracking
                        if (tempBounds.getBounds().contains(hero.getX() + (hero.getWidth() - 20), (hero.getY() + (hero.getHeight() - 10)))) {
                            rightCorridorInBounds = true;
                            if (deBugBounds) System.out.println("we're in corridor bounds right");
                        }
//                        else {
//                            if (deBugBounds) System.out.println("we're out of bounds right");
//                        }

                        if (tempBounds.contains(hero.getX() + 15, hero.getY() + (hero.getHeight() - 10))) {
                            leftCorridorInBounds = true;
                            if (deBugBounds) System.out.println("we're in corridor bounds left");
                        }
//                        else {
//                            if (deBugBounds) System.out.println("we're out of bounds left");
//                        }

                        if ((tempBounds.getBounds().contains(hero.getX() + (hero.getWidth() / 2), (hero.getY() + 20)))) {
                            upCorridorInBounds = true;
                            if (deBugBounds) System.out.println("we're in corridor bounds up");
                        }
//                        else {
//                            if (deBugBounds) System.out.println("we're out of bounds up");
//                        }

                        if ((tempBounds.getBounds().contains(hero.getX() + (hero.getWidth() / 2), (hero.getY() + hero.getHeight())))) {
                            downCorridorInBounds = true;
                            if (deBugBounds) System.out.println("we're in corridor bounds down");
                        }
//                        else {
//                            if (deBugBounds) System.out.println("we're out of bounds down");
//                        }
                    }
                }
                for (int i = 0; i < allCorridorBounds2[currentLevel].length; i++) {
                    if (deBug) System.out.println("Writing Corridor: " + i + " of " + allCorridorBounds2.length);
                    Rectangle tempBounds = allCorridorBounds2[currentLevel][i];

                    if (tempBounds != null) {
                        int X = (int) tempBounds.getX();
                        int Y = (int) tempBounds.getY();
                        g.setColor(new Color(50, 205, 50));
                        g.fillRect(X, Y, (int) tempBounds.getWidth(), (int) tempBounds.getHeight());

                        // testing location tracking
                        if (tempBounds.getBounds().contains(hero.getX() - 20 + hero.getWidth(), (hero.getY() + (hero.getHeight() - 10)))) {
                            rightCorridorInBounds = true;
                            if (deBugBounds) System.out.println("we're in corridor2 bounds right");
                        }
//                        else {
//                            if (deBugBounds) System.out.println("we're out of bounds right");
//                        }

                        if (tempBounds.contains(hero.getX() + 15, hero.getY() + (hero.getHeight() - 10))) {
                            leftCorridorInBounds = true;
                            if (deBugBounds) System.out.println("we're in corridor2 bounds left");
                        }
//                        else {
//                            if (deBugBounds) System.out.println("we're out of bounds left");
//                        }

                        if ((tempBounds.getBounds().contains(hero.getX() + (hero.getWidth() / 2), (hero.getY() + 20)))) {
                            upCorridorInBounds = true;
                            if (deBugBounds) System.out.println("we're in corridor2 bounds up");
                        }
//                        else {
//                            if (deBugBounds) System.out.println("we're out of bounds up");
//                        }

                        if ((tempBounds.getBounds().contains(hero.getX() + (hero.getWidth() / 2), (hero.getY() + hero.getHeight())))) {
                            downCorridorInBounds = true;
                            if (deBugBounds) System.out.println("we're in corridor2 bounds down");
                        }
//                        else {
//                            if (deBugBounds) System.out.println("we're out of bounds down");
//                        }
                    }

                }
            }
        }
        // if we have rooms to paint, do it
        if (allTheLevels != null) {
            if (deBug) System.out.println("level " + currentLevel);
            if (deBug) System.out.println("roomSize " + roomSize);

            for (int j = 0; j < roomSize; j++) {
                Rectangle roomBounds = allTheLevels[currentLevel][j];
                if (deBug) System.out.println("printing room " + j + " " + roomBounds);
                int X = (int) roomBounds.getX();
                int Y = (int) roomBounds.getY();
                int maxX = (int) roomBounds.getMaxX();
                int maxY = (int) roomBounds.getMaxY();
                g.drawImage(roomTile,
                        X, Y, maxX, maxY, /* dst rectangle */
                        0, 0, roomTileH, roomTileW, /* src area of image */
                        null);
                // add up and down stairs
                if (currentLevel == 0 && j == (roomSize - 1)) {
                    // first level, last room only gets up stairs
                    if (deBug) System.out.println("adding stairs up to first level last room");
                    X = (int) roomBounds.getCenterX();
                    Y = (int) roomBounds.getCenterY();
                    maxX = X + upStairsTileW;
                    maxY = Y + upStairsTileH;
                    upStairsRect = new Rectangle(X, Y, upStairsTileW, upStairsTileH);
                    g.drawImage(upStairsTile,
                            X, Y, maxX, maxY, /* dst rectangle */
                            0, 0, upStairsTileW, upStairsTileH, /* src area of image */
                            null);

                } else if (currentLevel == (allTheLevels.length - 1) && j == 0) {
                    // last level only gets up stairs
                    if (deBug) System.out.println("adding stairs down to last level first room");
                    X = (int) roomBounds.getCenterX();
                    Y = (int) roomBounds.getCenterY();
                    maxX = X + downStairsTileW;
                    maxY = Y + downStairsTileH;
                    downStairsRect = new Rectangle(X, Y, upStairsTileW, upStairsTileH);
                    g.drawImage(downStairsTile,
                            X, Y, maxX, maxY, /* dst rectangle */
                            0, 0, downStairsTileW, downStairsTileH, /* src area of image */
                            null);
                } else if ((currentLevel != 0 && currentLevel != (allTheLevels.length - 1)) && (j == (roomSize - 1))) {
                    // if the current level is not 0 or the last level, and the room is the first or last room.
                    // place all the stairs except level 0 and last room OR
                    if (deBug) System.out.println("adding stairs up");
                    X = (int) roomBounds.getCenterX();
                    Y = (int) roomBounds.getCenterY();
                    maxX = X + upStairsTileW;
                    maxY = Y + upStairsTileH;
                    upStairsRect = new Rectangle(X, Y, upStairsTileW, upStairsTileH);
                    g.drawImage(upStairsTile,
                            X, Y, maxX, maxY, /* dst rectangle */
                            0, 0, upStairsTileW, upStairsTileH, /* src area of image */
                            null);
                } else if ((currentLevel != 0 && currentLevel != (allTheLevels.length - 1)) && (j == 0)) {
                    if (deBug) System.out.println("adding stairs down");
                    X = (int) roomBounds.getCenterX();
                    Y = (int) roomBounds.getCenterY();
                    maxX = X + downStairsTileW;
                    maxY = Y + downStairsTileH;
                    downStairsRect = new Rectangle(X, Y, upStairsTileW, upStairsTileH);
                    g.drawImage(downStairsTile,
                            X, Y, maxX, maxY, /* dst rectangle */
                            0, 0, downStairsTileW, downStairsTileH, /* src area of image */
                            null);
                }

                if ((currentLevel == (allTheLevels.length - 1)) && (j == (roomSize - 1))) {
                    // last room of last level gets END GAME tile
                    if (deBug) System.out.println("Adding END GAME Parts");
                    X = (int) roomBounds.getCenterX();
                    Y = (int) roomBounds.getCenterY();

                    if (!DrEvilAdded && !endGameBool) {
                        DrEvilPacket.setLocation(X - 15, Y - 30);
                        DrEvilAdded = true;
                    }
                } else {
                    DrEvilPacket.setLocation(-400,-300);
                    DrEvilAdded = false;
                }


                // testing location tracking
                if ((allTheLevels[currentLevel][j].getBounds().contains(hero.getX() + hero.getWidth() - 20, (hero.getY() + (hero.getHeight() - 10))))) {
                    rightInBounds = true;
                    if (deBugBounds) System.out.println("we're in room bounds right");
                }
//                else {
//                    if (deBugBounds) System.out.println("we're out of bounds right");
//                }

                if ((allTheLevels[currentLevel][j].getBounds().contains(hero.getX() + 15, hero.getY() + (hero.getHeight() - 10)))) {
                    leftInBounds = true;
                    if (deBugBounds) System.out.println("we're in room bounds left");
                }
//                else {
//                    if (deBugBounds) System.out.println("we're out of bounds left");
//                }

                if ((allTheLevels[currentLevel][j].getBounds().contains(hero.getX() + (hero.getWidth() / 2), hero.getY() + 20))) {
                    upInBounds = true;
                    if (deBugBounds) System.out.println("we're in room bounds up");
                }
//                else {
//                    if (deBugBounds) System.out.println("we're out of bounds up");
//                }

                if ((allTheLevels[currentLevel][j].getBounds().contains(hero.getX() + (hero.getWidth() / 2), (hero.getY() + hero.getHeight())))) {
                    downInBounds = true;
                    if (deBugBounds) System.out.println("we're in room bounds down");
                }
//                else {
//                    if (deBugBounds) System.out.println("we're out of bounds down");
//                }


                if (upStairsRect != null && upStairsRect.contains((hero.getX() + (hero.getWidth() / 2)), (hero.getY() + (hero.getHeight() / 2)))) {
                    if (deBugBounds) System.out.println("Hero is on Up Stairs");
                    setNextLevel();
                }
                if (downStairsRect != null && downStairsRect.contains((hero.getX() + (hero.getWidth() / 2)), (hero.getY() + (hero.getHeight() / 2)))) {
                    if (deBugBounds) System.out.println("Hero is on Down Stairs");
                    setPrevLevel();
                }
            }

            // set the booleans based on our tests
            if (!endGameBool) {
                // dont move right if not in right bounds
                dontMoveRight = !rightInBounds && !rightCorridorInBounds;
                // dont move left if not in leftbounds
                dontMoveLeft = !leftInBounds && !leftCorridorInBounds;
                // dont move up if not in up bounds
                dontMoveUp = !upInBounds && !upCorridorInBounds;
                // dont move downif not in down bounds
                dontMoveDown = !downInBounds && !downCorridorInBounds;
            }
            else {
                dontMoveRight = true;
                dontMoveLeft = true;
                dontMoveUp = true;
                dontMoveDown = true;
            }
        }
        if (deBugBounds) System.out.println("#######################################");

        // Print each treasure that should be visible
        for (int treasure = 0; treasure < monsterNumbers; treasure++) {
            if (levelTreasure[currentLevel][treasure].getTreasureVisible()) {
                if (deBugTreasure) System.out.println("Treasure should be visible");
                //add(levelTreasure[currentLevel][treasure]);
                levelTreasure[currentLevel][treasure].setBounds(levelTreasure[currentLevel][treasure].getTreasureLocation());
            } else {
                if (deBugTreasure) System.out.println("Treasure should NOT be visible");
            }
        }

        if (hero != null) {
            updateHero(hero.getBounds());
        }

    }

    private void updateMonsters() {
        if (levelMonsters != null && levelMonstersLocation != null) {
            // asume not near monsters
            monsterNear = false;
            // Add all the monsters
            for (int monster = 0; monster < monsterNumbers; monster++) {
                // get monster location and size (bounds)
                Rectangle monsterLoc = levelMonstersLocation[currentLevel][monster].getBounds();
                // Print if we're debugging
                if (deBug)
                    System.out.println("Printing monster " + monster + " on screen for level " + currentLevel + " at location " + monsterLoc);
                // if we have not yet added monsters, do that next
                if (!currentLevelMonsterAdded[currentLevel]) {
                    // add current monster
                    add(levelMonsters[currentLevel][monster]);
                    // if health is more than 0, display them
                    if (levelMonsters[currentLevel][monster].getHealth() > 0) {
                        // set location to defined bounds
                        levelMonsters[currentLevel][monster].setBounds(monsterLoc);
                        // if we need to place health bars, do that now
                        if (settings.getMonsterHealthBarVisable()) {
                            // add to panel
                            add(monsterHealth[currentLevel][monster]);
                            // set it's location
                            monsterHealth[currentLevel][monster].setBounds(monsterHealthLocation[currentLevel][monster]);
                            // set it's current health
                            monsterHealth[currentLevel][monster].setValue(levelMonsters[currentLevel][monster].getHealth());
                        }
                    } else {
                        // if monster is already dead, don't add them
                        if (deBug) System.out.println("This Monster is dead: " + monster);
                        // remove from panel
                        levelMonsters[currentLevel][monster].setLocation(-400, -200);
                        // if health bar is visible, get rid of it
                        if (settings.getMonsterHealthBarVisable()) {
                            // set location off panel
                            monsterHealth[currentLevel][monster].setLocation(-400, -200);
                            // set value for the heck of it
                            monsterHealth[currentLevel][monster].setValue(levelMonsters[currentLevel][monster].getHealth());
                        }
                    }
                } else {
                    // if debugging print it
                    if (deBug) System.out.println("Monsters have been added already");
                }
                if (levelMonsters[currentLevel][monster].getBounds().intersects(hero.getBounds())) {
                    // Only do damage to hero when Monster reaches end of attack animation
                    Boolean doDamge;
                    monsterNear = true;
                    currentMonster = levelMonsters[currentLevel][monster];
                    currentMonsterInt = monster;
                    if (currentMonster.getHealth() > 0) {
                        if (deBug) System.out.println("Hero has entered the battle field for monster " + monster);
                        if (hero.getX() < currentMonster.getX()) {
                            // attack left
                            doDamge = currentMonster.attackLeft();
                        } else {
                            // attack right
                            doDamge = currentMonster.attackRight();
                        }
                        int damage;
                        if (doDamge) {
                            damage = makeRandomInt((makeRandomInt(((currentLevel + 1)  * 5) + 3)));
                            System.out.println("Monster damage: " + damage);
                            if (damage > 0) {
                                monsterDamage.playMusic();
                                updateTextConsole("<font color=\"#ff9999\">" + currentMonster.getSpriteName() + " damaged " + hero.getSpriteName() + " with " + damage + " damage</font>");
                                if (deBug)
                                    System.out.println(currentMonster.getSpriteName() + " damaged " + hero.getSpriteName() + " with " + damage + " damage");
                                hero.reduceHealth(damage);
                            } else {
                                swordMiss.playMusic();
                                updateTextConsole("<font color=\"#ff9999\">" + currentMonster.getSpriteName() + " missed " + hero.getSpriteName() + "</font>");
                            }
                        }
                    } else {
                        if (deBug) System.out.println("This Monster is Dead!");
                        if (deBug) System.out.println("Adding Treasure");
                        addTreasure(currentLevel, monster);
                        // moving monster off screen
                        currentMonster.setLocation(-400, -200);
                        if (settings.getMonsterHealthBarVisable()) {
                            monsterHealth[currentLevel][monster].setLocation(-400, -200);
                        }
                        // adding points to hero's score
                        hero.increaseHeroPoints(monsterPoints);
                        monsterNear = false;
                    }
                } else {
                    // otherwise set monster to forward facing
                    levelMonsters[currentLevel][monster].returnToDefaultImage();
                    if (deBug) System.out.println("Not near monster " + monster);
                }

            }
            currentLevelMonsterAdded[currentLevel] = true;
        }
        if (DrEvilAdded) {
            if (DrEvilPacket.getBounds().intersects(hero.getBounds())) {
                // Only do damage to hero when Monster reaches end of attack animation
                // drEvilPacket.loopMusic(1);
                Boolean doDamge;
                DrEvilNear = true;
                currentMonster = DrEvilPacket;
                if (DrEvilPacket.getHealth() > 0) {
                    if (deBug)
                        System.out.println("Hero has entered the battle field with " + DrEvilPacket.getSpriteName());
                    if (hero.getX() < DrEvilPacket.getX()) {
                        // attack left
                        doDamge = DrEvilPacket.attackLeft();
                    } else {
                        // attack right
                        doDamge = DrEvilPacket.attackRight();
                    }
                    int damage = makeRandomInt(((currentLevel + 1) * 5) + 5);
                    if (doDamge) {
                        if (damage > 0) {
                            monsterDamage.playMusic();
                            updateTextConsole("<font color=\"#ff9999\">" + DrEvilPacket.getSpriteName() + " damaged " + hero.getSpriteName() + " with " + damage + " damage</font>");
                            hero.reduceHealth(damage);
                        } else {
                            swordMiss.playMusic();
                            updateTextConsole("<font color=\"#ff9999\">" + DrEvilPacket.getSpriteName() + " missed " + hero.getSpriteName() + " !</font>");
                        }
                    }
                } else {
                    // adding points to hero's score
                    hero.increaseHeroPoints(drevilPoints);
                    DrEvilPacket.setLocation(-400, -200);
                    if (deBug) System.out.println("DrEvil Added and dead, end game");
                    endGame(0);
                }
            } else {
                DrEvilNear = false;
            }

        }

    }
    private void addTreasure(int currentLevel, int monster) {
        Treasure newTreasure = levelTreasure[currentLevel][monster];
        add(newTreasure);
        Rectangle tempTreasureLocation = new Rectangle((currentMonster.getX() + (currentMonster.getWidth() / 2)), (currentMonster.getY() + (currentMonster.getHeight() / 2)), newTreasure.getWidth(), newTreasure.getHeight());
        if (deBugTreasure) System.out.println("Adding Treasure at " + (currentMonster.getX() + (currentMonster.getWidth() / 2)) + "," + (currentMonster.getY() + (currentMonster.getHeight() / 2)) +","+ newTreasure.getWidth() +","+ newTreasure.getHeight());
        newTreasure.setTreasureLocation(tempTreasureLocation);
        newTreasure.setTreasureVisible(true);
        newTreasure.setBounds(tempTreasureLocation);

    }

    void updateHero(Rectangle currentSpot) {
        for (int treasure = 0; treasure < monsterNumbers; treasure++) {
            if (deBug) System.out.println("Checking Treasure Locations");
            // if our hero rectangle contains the smaller treasure rectangle, pick it up.
            if (new Rectangle((hero.getX() +  14), (hero.getY()), 40, 64).contains(levelTreasure[currentLevel][treasure].getBounds()) ) {
                treasurePickUp.playMusic();
                if (deBugBounds) System.out.println("Hero is over a treasure");
                levelTreasure[currentLevel][treasure].setLocation(-100,-100);
                levelTreasure[currentLevel][treasure].setTreasureVisible(false);

                // get the treasure number type
                //{"Health Potion","Speed Potion","5Ghz Wireless Router","PortFast Packet"}
                switch (levelTreasure[currentLevel][treasure].getTreasureType()) {
                    case 0:
                        // Health Potion
                        if (deBug) System.out.println("Hero current health " + hero.getHealth());
                        if (deBug) System.out.println("Hero max health " + hero.getMaxHealth());
                        // add random number 1-15
                        int randomHealth = makeRandomInt(15) + 1;
                        hero.increaseHealth(randomHealth);
                        updateTextConsole("<font color=\"#99ddff\">[" + hero.getSpriteName() + " health increased by " + randomHealth + " to " + hero.getHealth() + " of " + hero.getMaxHealth() + "]</font>");
                        // adding points to hero's score
                        hero.increaseHeroPoints(treasurePoints);
                        break;
                    case 1:
                        // Speed Potion
                        if (deBug) System.out.println("Hero current attack speed " + hero.getAttackSpeed());
                        // add random number 1-15
                        int randomSpeed = makeRandomInt(5) + 1;
                        //hero.increaseMoveSpeed(randomSpeed);
                        hero.increaseAttackSpeed(randomSpeed);
                        heroAnimationTimer.setDelay(hero.getAttackSpeed());
                        updateTextConsole("<font color=\"#99ddff\">[" + hero.getSpriteName() + " attack speed increased by " + randomSpeed + " to " + (100 - hero.getAttackSpeed()) + "]</font>");
                        // adding points to hero's score
                        hero.increaseHeroPoints(treasurePoints);
                        break;
                    case 2:
                        // 5Ghz Wireless Router
                        if (deBug) System.out.println("Hero found level skip");
                        // if we're not on the last level
                        if (currentLevel != (numberOfLevels - 1)) {
                            setNextLevel();
                            updateTextConsole("<font color=\"#99ddff\">[" + hero.getSpriteName() + " has been transported to the next level" + "]</font>");
                        } else {
                            updateTextConsole("<font color=\"#99ddff\">[" + hero.getSpriteName() + " is already on the last level" + "]</font>");
                        }
                        // adding points to hero's score
                        hero.increaseHeroPoints(treasurePoints);
                        break;
                    case 3:
                        // PortFast Packet
                        if (deBug) System.out.println("Hero attack power " + hero.getAttackDamage());
                        // add random number 1-15
                        int randomAttack = makeRandomInt(5) + 1;
                        // increase attack damage
                        hero.increaseAttackDamage(randomAttack);
                        updateTextConsole("<font color=\"#99ddff\">[" + hero.getSpriteName() + " attack damage increased by " + randomAttack + " to " + hero.getAttackDamage() + "]</font>");
                        // adding points to hero's score
                        hero.increaseHeroPoints(treasurePoints);
                        break;
                }
                updateTextConsole("<font color=\"#99ff99\">Hero found a " + levelTreasure[currentLevel][treasure].getTreasureName() + "</font>");
            }
        }

        if (heroHealth != null) {
            heroHealth.setBounds((int) (currentSpot.getX() + 5), (int) (currentSpot.getY() + 5), heroHealthW, heroHealthH);
            heroHealth.setValue(hero.getHealth());
        }
        if (hero.getHealth() > 20) {
            heartBeat.stopMusic();
        } else if (hero.getHealth() <= 20 && hero.getHealth() > 1) {
            heartBeat.loopMusic(1);
        } else {
            heartBeat.stopMusic();
            endGame(1);
        }


    }

    void setSkipLoop(Boolean newBool) {
        skipLoop = newBool;
    }

    Boolean getSkipLoop() {
        return skipLoop;
    }

    void setNextLevel() {
        if (currentLevel < (numberOfLevels - 1)) {
            doorOpens.playMusic();
            // clear out the monsters so we can reprint them
            for (int level = 0; level < (numberOfLevels - 1); level++) {
                if (deBugBounds) System.out.println("Removed Level " + level + " of " + (numberOfLevels - 1));
                currentLevelMonsterAdded[level] = false;

                for (int monster = 0; monster < monsterNumbers; monster++) {
                    levelMonsters[level][monster].setLocation(-500, -500);
                    if (settings.getMonsterHealthBarVisable()) {
                        monsterHealth[level][monster].setLocation(-500, -500);
                    }
                    if (levelTreasure[level][monster].getTreasureVisible()) {
                        levelTreasure[level][monster].setLocation(-500, -500);
                    }
                }
            }
            DrEvilAdded = false;
            currentLevel += 1;
            if (deBug) System.out.println("Increasing Current Level " + currentLevel);
            hero.setBounds((int) allTheLevels[currentLevel][0].getCenterX() - 10, (int) allTheLevels[currentLevel][0].getCenterY() - 40, hero.getWidth(), hero.getHeight());

            repaint();
            // set focus of keyboard to mainframe
            mainFrame.requestFocusInWindow();
        }
    }

    void setPrevLevel() {
        if (currentLevel > 0) {
            doorOpens.playMusic();
            // clear out the monsters so we can reprint them
            for (int level = 0; level < numberOfLevels; level++) {
                for (int monster = 0; monster < monsterNumbers; monster++) {
                    levelMonsters[level][monster].setLocation(-500, -500);
                    if (settings.getMonsterHealthBarVisable()) {
                        monsterHealth[level][monster].setLocation(-500, -500);
                    }
                    if (levelTreasure[level][monster].getTreasureVisible()) {
                        levelTreasure[level][monster].setLocation(-500, -500);
                    }
                }
                currentLevelMonsterAdded[level] = false;
            }
            DrEvilAdded = false;
            currentLevel -= 1;
            if (deBug) System.out.println("Decreasing Current Level " + currentLevel);
            hero.setBounds((int) allTheLevels[currentLevel][roomSize - 1].getCenterX() - 10, (int) allTheLevels[currentLevel][roomSize - 1].getCenterY() + 5, hero.getWidth(), hero.getHeight());
            repaint();
            // set focus of keyboard to mainframe
            mainFrame.requestFocusInWindow();
        }
    }


    void endGame(int endType) {

        if (!endGameBool) {

            gameTimer.stop();
            monsterAnimationTimer.stop();
            heroAnimationTimer.stop();

            JLabel endGame = new JLabel();
            JLabel gameText = new JLabel();
            String[] highName = new String[5];
            int[] highScore = new int[5];
            // create new save file
            xmlSave = new XML_240();
            // read XML settings
            // XML File saved as
            File xmlCheck = new File(xmlFile);
            if (xmlCheck.exists()) {
                if (deBug) System.out.println("Loading XML file");
                xmlSave.openReaderXML(xmlFile);
                // String object
                highName[0] = (String) xmlSave.ReadObject();
                highScore[0] = (int) xmlSave.ReadObject();
                highName[1] = (String) xmlSave.ReadObject();
                highScore[1] = (int) xmlSave.ReadObject();
                highName[2] = (String) xmlSave.ReadObject();
                highScore[2] = (int) xmlSave.ReadObject();
                highName[3] = (String) xmlSave.ReadObject();
                highScore[3] = (int) xmlSave.ReadObject();
                highName[4] = (String) xmlSave.ReadObject();
                highScore[4] = (int) xmlSave.ReadObject();
                xmlSave.closeReaderXML();
            } else {
                // if there is no high score list, create an empty one
                for (int i = 0; i < 5; i++) {
                    highName[i] = "none";
                    highScore[i] = 0;
                }
            }
            int heroSpot = 6;
            int heroScore = hero.getHeroPoints();
            for (int i = 0; i < 5; i++) {
                if (deBug)
                    System.out.println("Checking Hero Score " + heroScore + " against top score " + i + " : " + highScore[i]);
                if (heroScore > highScore[i]) {
                    if (deBug) System.out.println("Hero score is greater than high score " + i);
                    heroSpot = i;
                    break;
                }
            }

            for (int i = 4; i >= heroSpot; i--) {
                if (deBug) System.out.println("Updating high scores starting with " + i);
                if (i != heroSpot) {
                    if (deBug)
                        System.out.println("high score " + highScore[i - 1] + " in spot " + i + " should be placed into spot: " + i);
                    highScore[i] = highScore[i - 1];
                    highName[i] = highName[i - 1];
                } else {
                    if (deBug) System.out.println("Hero has this spot: " + i);
                    highName[i] = hero.getSpriteName();
                    highScore[i] = hero.getHeroPoints();
                }
            }

            // write out updated scores
            xmlSave.openWriterXML(xmlFile);
            xmlSave.writeObject(highName[0]);
            xmlSave.writeObject(highScore[0]);
            xmlSave.writeObject(highName[1]);
            xmlSave.writeObject(highScore[1]);
            xmlSave.writeObject(highName[2]);
            xmlSave.writeObject(highScore[2]);
            xmlSave.writeObject(highName[3]);
            xmlSave.writeObject(highScore[3]);
            xmlSave.writeObject(highName[4]);
            xmlSave.writeObject(highScore[4]);

            // save file
            xmlSave.closeWriterXML();

            // remove all monsters and treasure
            for (int monster = 0; monster < monsterNumbers; monster++) {
                // remove all the treasure
                levelTreasure[currentLevel][monster].setLocation(-500, -500);
                levelTreasure[currentLevel][monster].setTreasureVisible(false);
                // remove all the monsters
                levelMonsters[currentLevel][monster].setLocation(-500, -500);
                // remove health bars for monsters
                if (settings.getMonsterHealthBarVisable()) {
                    monsterHealth[currentLevel][monster].setLocation(-500, -500);
                }
            }
            // bye bye evil packet
            DrEvilPacket.setLocation(-400, -200);

            add(endGame);
            gameText.setForeground(Color.WHITE);

            if (endType == 0) {
                victory.playMusic();
                hero.setLocation((mainFrame.getWidth() / 2 - (390)), 210);
                gameText.setText("<html>   YOU WIN! Hero "
                        + hero.getSpriteName()
                        + " has " + hero.getHealth()
                        + " health of "
                        + hero.getMaxHealth()
                        + " max.<br>"
                        + "    Attack speed was " + (100 - hero.getAttackSpeed() + ".<br>")
                        + "    Attack power was " + hero.getAttackDamage() + ".<br>"
                        + "    You have played for " + (loop / 60) + " minutes.<br>"
                        + "    The Final Score is " + hero.getHeroPoints()
                        + "</html>");
                // Configure the gameText
                gameText.setFont(new Font("Serif", Font.PLAIN, 16));

                JLabel defeated = new JLabel("Dr. Evil Has Been Defeated!");
                defeated.setForeground(Color.WHITE);
                defeated.setFont(new Font("Serif", Font.PLAIN, 20));
                defeated.setBounds(100, 10, 400, 80);

                endGame.add(defeated);

            } else if (endType == 1) {
                // defeated sound file goes here
                defeatedSound.playMusic();
                if (settings.getHeroHealthBarVisable()) {
                    heroHealth.setLocation(-300, -200);
                }
                hero.setLocation((mainFrame.getWidth() / 2 - (390)), 210);
                // end game with death (option 1)
                gameText.setText("<html>   YOU LOOSE! Hero "
                        + hero.getSpriteName()
                        + " has died!<br> "
                        + "    Attack speed was " + (100 - hero.getAttackSpeed() + ".<br>")
                        + "    Attack power was " + hero.getAttackDamage() + ".<br>"
                        + "    You have played for " + (loop / 60) + " minutes.<br>"
                        + "    The Final Score is " + hero.getHeroPoints()
                        + "</html>");
            }

            endGame.add(gameText);
            gameText.setBounds(10, 80, 400, 120);


            // set background settings
            endGame.setBackground(new Color(3, 4, 29));
            endGame.setOpaque(true);

            // configure the high scores label
            // use our method to get a new Jlabel from the highscores file on disk
            JLabel highScores = getHighScores(xmlFile);
            // add the scores to our endGame label
            endGame.add(highScores);
            // set the scores bounds
            highScores.setBounds(410, 80, 390, 200);
            // set the endgame bounds
            endGame.setBounds(mainFrame.getWidth() / 2 - (400), 200, 800, 400);
            // repaint so we see everything
            repaint();
            // dont do all this again!
            endGameBool = true;
        }
    }

    Boolean getDontMoveRight() {
    return dontMoveRight;}

    Boolean getLeftInBounds() {
    return dontMoveLeft;}

    Boolean getUpInBounds() {
    return dontMoveUp;}

    Boolean getDownInBounds() {
    return dontMoveDown;}

    void heroAttack() {heroAnimationTimer.start();}

    void startGameStory() {
        if ((newHeroNameField.getText().contentEquals("")) && (chosenHero.equals("none"))) {
            nameInstruction.setText("Select both a Charachter & a Name.");
        } else if (newHeroNameField.getText().contentEquals("")) {
            nameInstruction.setText("Set a Name first!");
        } else if (chosenHero.equals("none")) {
            nameInstruction.setText("Don't forget to select a Charachter!");
        } else {

            if (deBug) System.out.println("User has made their choice. Creating Hero and new levels");

            hero = new Hero(newHeroNameField.getText(), heroSexChoice);
            DrEvilPacket = new Monster("Dr. EvilPacket", makeRandomInt((numberOfLevels + 1) * 5) + 10, true);

            // when sex is selected, then
            gameBackground.remove(heroSetButton);
            gameBackground.remove(maleStart);
            gameBackground.remove(femaleStart);
            gameBackground.remove(textOutput);
            gameBackground.remove(newHeroNameField);
            gameBackground.remove(maleHero);
            gameBackground.remove(femaleHero);
            gameBackground.remove(nameInstruction);
            heroTimer.stop();

            // set layout to border for jlabel
            setLayout(new BorderLayout());

            // START CUSTOM PANEL WORK
            // create introduction jlabel
            introduction = new JLabel(
                    //Broken up by line for readability when changing
                    "<html><b>Once upon a time...</b><br><br>"
                            + "<b></b>"
                            + "there was an Network Admin named <b>"
                            + hero.getSpriteName()
                            + "</b> that cursed his switches.<br>"
                            + "Until one day, when his switches cursed him!<br><br>"
                            + "The Network Admin was suddenly digitized and found himself<br>"
                            + "stuck inside his network! Traveling through the patch cables to each<br>"
                            + "router, his only chance to return to a normal life is by defeating<br>"
                            + "the enemies and reaching the escape port at the end of the network!<br>"
                            + "<br>Watch out for <b>Dr. Evil Packet and his evil network minions!</b><br>"
                            + "<br><br>"
                            + "<b>Press Space to Skip...</b>"
                            + "</html>"
            );
            // Sets jlabel font color
            introduction.setForeground(Color.WHITE);
            introduction.setBackground(new Color(5, 5, 7));
            introduction.setOpaque(true);
            // Add introduction jlabel to castle jlabel
            gameBackground.add(introduction);

            // get preferred size of introduction label
            Dimension size = introduction.getPreferredSize();
            // set size and placement of introduction label- (starting x, starting y, x width, y width)
            introduction.setBounds(100, 200, size.width, size.height);

            // add label to display current level
            levelLabel = new JLabel("Level ");
            levelLabel.setForeground(Color.WHITE);

            // add label to display current damage
            textConsole = new JLabel(" ");
            textConsole.setForeground(Color.WHITE);

            repaint();

            gameTimer.start();
            addKeyListener(mainFrame);
            monsterAnimationTimer.start();
        }
    }

    void heroChoiceSetup(String newStringSex, int newSexChoice) {
        chosenHero = newStringSex;
        heroSexChoice = newSexChoice;
        heroTimer.start();
    }
    void updateTextConsole(String newText) {
        messageBuffer = (newText + "<br>" + messageBuffer);
        if (deBug) System.out.println("Updated buffer: " + messageBuffer);
        String textUpdate = ("<html><font size=\"-2\">" + messageBuffer + "</font></html>");
        if (deBug) System.out.println("Updated String: " + textUpdate);
        textConsole.setText(textUpdate);
    }

    JLabel getHighScores(String newxmlFile) {
        if (deBug) System.out.println("Getting high scores");
        JLabel viewHighScores = new JLabel();
        String[] highName = new String[5];
        int[] highScore = new int[5];
        // create new save file
        xmlSave = new XML_240();
        // read XML settings
        // XML File saved as
        File xmlCheck = new File(newxmlFile);
        if (xmlCheck.exists()) {
            if (deBug) System.out.println("Loading XML file");
            xmlSave.openReaderXML(newxmlFile);
            // String object
            highName[0] = (String) xmlSave.ReadObject();
            highScore[0] = (int) xmlSave.ReadObject();
            highName[1] = (String) xmlSave.ReadObject();
            highScore[1] = (int) xmlSave.ReadObject();
            highName[2] = (String) xmlSave.ReadObject();
            highScore[2] = (int) xmlSave.ReadObject();
            highName[3] = (String) xmlSave.ReadObject();
            highScore[3] = (int) xmlSave.ReadObject();
            highName[4] = (String) xmlSave.ReadObject();
            highScore[4] = (int) xmlSave.ReadObject();
            xmlSave.closeReaderXML();
        } else {
            // if there is no high score list, create an empty one
            for (int i = 0; i < 5; i++) {
                highName[i] = "none";
                highScore[i] = 0;
            }
        }
        viewHighScores.setForeground(Color.WHITE);
        viewHighScores.setFont(new Font("Serif", Font.PLAIN, 24));

        viewHighScores.setText("<html>"
                        + "<b>High Scores</b><br>"
                        + "1: " + highName[0] + " scored " + highScore[0] + "<br>"
                        + "2: " + highName[1] + " scored " + highScore[1] + "<br>"
                        + "3: " + highName[2] + " scored " + highScore[2] + "<br>"
                        + "4: " + highName[3] + " scored " + highScore[3] + "<br>"
                        + "5: " + highName[4] + " scored " + highScore[4] + "<br>"
                        + ""
                        + ""
                        + "</html>"
        );
        return viewHighScores;
    }

    void showHighScores(Boolean doWhat) {
        if (!doWhat){
            System.out.println("Removing high scores");
            remove(viewHighScores);
            repaint();
            requestFocusInWindow();
            panelAdded = false;
        } else {
            System.out.println("Adding high scores");
            viewHighScores = getHighScores(xmlFile);
            add(viewHighScores);
            viewHighScores.setFont(new Font("Serif", Font.PLAIN, 16));
            viewHighScores.setBounds(1120, 480, 390, 200);
            repaint();
            requestFocusInWindow();
            panelAdded = true;
        }
    }

    //    void setLevel(int newLevelInt) {
//
//        if ((currentLevel < (numberOfLevels-1)) &&  (currentLevel > 0)) {
//            // clear out the monsters so we can reprint them
//            for (int level = 0; level < (numberOfLevels-1); level++) {
//                System.out.println("Removed Level " + level + " of " + (numberOfLevels-1));
//                currentLevelMonsterAdded[level] = false;
//
//                for (int monster = 0; monster < monsterNumbers; monster++) {
//                    levelMonsters[level][monster].setLocation(-500, -500);
//                    if (settings.getMonsterHealthBarVisable()) {
//                        monsterHealth[level][monster].setLocation(-500, -500);
//                    }
//                }
//            }
//            currentLevel = newLevelInt;
//            if (deBug) System.out.println("Setting Level " + currentLevel);
//            hero.setBounds((int) allTheLevels[currentLevel][0].getCenterX()-10, (int) allTheLevels[currentLevel][0].getCenterY() - 40, hero.getWidth(), hero.getHeight());
//
//            repaint();
//            // set focus of keyboard to mainframe
//            mainFrame.requestFocusInWindow();
//        }
//
//    }
}
