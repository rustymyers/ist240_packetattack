/**
 * @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: MainMenu.java
 * Short Description: Main Menu for Final Project
*/
import java.awt.*;
import javax.swing.*;

public class MainMenu extends JPanel
{
	// Class parameters
	public JButton startButton,instructionButton,settingButton,creditButton,quitButton;
	public JLabel titleText;

	public MainMenu()
	{
		// Call superclass for Panel
		super();
		// Set panel layout to Border
		setLayout(new BorderLayout());
		// create local variable for use
		Dimension size;
		// Create image icon from local file
		ImageIcon castleImage = new ImageIcon(this.getClass().getResource("images/Flat_earth_night.jpg"));
		// Add scaled castle image to JLabel
		JLabel castleBackground = new JLabel(castleImage);
		// Add new castle image jlabel to menu panel
		add(castleBackground);
		// set castle image jlabel layout to Null
		castleBackground.setLayout(null);

		// Create Game Title Label
		titleText = new JLabel("<html><b>Packet Attack</b><html>");
		castleBackground.add(titleText);
		// Sets jlabel font color
		titleText.setForeground(Color.WHITE);
		// set font and size
		titleText.setFont(new Font("Serif", Font.ITALIC, 60));

		// Create Buttons for Main Menu
		startButton = new JButton("Start Game");
		// Add button to castle jlabel
		castleBackground.add(startButton);
		
		// Create Button for Instructions Panel
		instructionButton = new JButton("Instructions");
		// Add button to castle jlabel
		castleBackground.add(instructionButton);
		
		// Create button for Settings Panel
		settingButton = new JButton("Settings");
		// Add button to castle jlabel
		castleBackground.add(settingButton);
		
		// Create button for Credits Panel
		creditButton = new JButton("Credits");
		// Add button to castle jlabel
		castleBackground.add(creditButton);
		
		// Create button for Quiting
		quitButton = new JButton("Quit");
		// Add button to castle jlabel
		castleBackground.add(quitButton);
		
		// Set size and layout of each button on castle label, reuse size variable
		// Title Text
		size = titleText.getPreferredSize();
		titleText.setBounds(640-(size.width/2),150,size.width, size.height);
		// start button
		size = startButton.getPreferredSize();
		// set size and placement of start button - (starting x, starting y, x width, y width)
		startButton.setBounds(640-(size.width/2),450,size.width, size.height);
		// instructions button
		size = instructionButton.getPreferredSize();
		// set size and placement of instructions button - (starting x, starting y, x width, y width)
		instructionButton.setBounds(440-(size.width/2),550,size.width, size.height);
		// settings button
		size = settingButton.getPreferredSize();
		// set size and placement of settings button - (starting x, starting y, x width, y width)
		settingButton.setBounds(640-(size.width/2),550,size.width, size.height);
		// credit button
		size = creditButton.getPreferredSize();
		// set size and placement of credit button - (starting x, starting y, x width, y width)
		creditButton.setBounds(840-(size.width/2),550,size.width, size.height);
		// quit button
		size = quitButton.getPreferredSize();
		// set size and placement of quit button - (starting x, starting y, x width, y width)
		quitButton.setBounds(640-(size.width/2),650,size.width, size.height);
	}	
}