/**
 * @author Rusty Myers, Joshua Mathews
 * @version 0.1
 * Filename: Treasure.java
 * Short Description: Treasure Class for Final Project
 * Date: 10/15/15
 **/

import javax.swing.*;
import java.awt.*;

public class Treasure extends JLabel {
    Boolean deBug = false;
    Boolean treasureVisible;
    String name;
    ImageIcon treasureImageIcon;
	int treasureType;
	String[] treasureName = new String[] {"Health Potion","Speed Potion","5Ghz Wireless Router","PortFast Packet"};
    Rectangle treasureLocation;

    // construct new sprite image
    Treasure() {
        super();
        // set button background to false unless debugging
        if (deBug) {
            // print debug is on
            System.out.println("Debug On");
            // set border to painted
        }
        // set it to visible
        treasureVisible = false;

        treasureType = makeRandomInt(4);

        // setup the rest
        setupTreasure();

    }

    // construct new sprite image
    Treasure(String newName, int newType) {
        super();
        // set button background to false unless debugging
        if (deBug) {
            // print debug is on
            System.out.println("Debug On");
            // set border to painted
        }
        // set it to visible
        treasureVisible = false;

        // set the type to the
        treasureType = newType;

        // setup the rest
        setupTreasure();

        // set name from received value
        name = newName;
    }

    void setupTreasure() {
        // set name from received value
        name = treasureName[treasureType];

        // setup the treasure
        switch (treasureType) {
            case 0:
                // health potion
                // increases health
                treasureImageIcon = new ImageIcon(this.getClass().getResource("treasure/healthpotion.png"));
                setIcon(treasureImageIcon);
                setSize(25, 25);
                break;
            case 1:
                // speed potion
                // increases attack Speed
                treasureImageIcon = new ImageIcon(this.getClass().getResource("treasure/speed.png"));
                setIcon(treasureImageIcon);
                setSize(25, 26);
                break;
            case 2:
                // 5Ghz Wireless Router
                // Increases Damage
                treasureImageIcon = new ImageIcon(this.getClass().getResource("treasure/wifi.png"));
                setIcon(treasureImageIcon);
                setSize(25, 25);
                break;
            case 3:
                // PortFast Packet
                // Skips a level!
                treasureImageIcon = new ImageIcon(this.getClass().getResource("treasure/portfast.png"));
                setIcon(treasureImageIcon);
                setSize(25, 25);
                break;
        }
    }




    String getTreasureName() {
        return name;
    }

    int getTreasureType() {
        return treasureType;
    }
    // Create and return a random int. Pass a range to it
    int makeRandomInt(double range)
    {
        // create random value
        double r = Math.random();
        // create new value
        int myrandomNumber;
        // Create range between 0 and 5
        myrandomNumber = (int)(r*range);
        // Return value
        return myrandomNumber;
    }

    Boolean getTreasureVisible() {
        return treasureVisible;
    }

    void setTreasureVisible(Boolean newVisible) {
        treasureVisible = newVisible;
    }

    void setTreasureLocation(Rectangle newLocation) {
        treasureLocation = newLocation;
    }

    Rectangle getTreasureLocation() {
        if (treasureLocation != null) {
            return treasureLocation;
        } else {
            return new Rectangle(0, 0, 0, 0);
        }
    }

//    // set the image that is currently displayed
//    void setTreasureImage(ImageIcon newImageIcon) {
//        setIcon(newImageIcon);
//    }
//
//    // set default image
//    void setTreasureImageIcon(ImageIcon newTreasureImageIcon) {
//        treasureImageIcon = newTreasureImageIcon;
//    }
//
//    // get treasure image
//    ImageIcon getTreasureImageIcon() {
//        return treasureImageIcon;
//    }
//
//    // Update name of sprite
//    void setSpriteName(String newName) {
//        name = newName;
//    }

}
